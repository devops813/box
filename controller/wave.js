// const commonmodels = require('@leadercodes/modelsnpm');
// const User = commonmodels.models.user
// const Conversation = commonmodels.models.conversation
// const Wave = commonmodels.models.wave
// const Contact = commonmodels.models.contact
// const SystemWaves = commonmodels.models.systemWaves


const Contact = require('../models/Contact');
const Wave = require('../models/Wave');
const Conversation = require('../models/Conversation');
const User = require('../models/User')
const SystemWaves = require('../models/SystemWave')
const request = require('request')


module.exports = {

    getSystemWaves: (req, res) => {
        console.log("inside getSeystemWaves")
        const userName = req.params.username
        console.log(userName)
        User.findOne({ username: userName })
            .populate('systemWaves')
            .then((user) => {
                const systemWaves = user.systemWaves
                res.status(200).json({
                    systemWaves
                })
            }).catch(error => {
                console.log("error in get system waves", error)
                res.status(500).json({
                    error
                })
            })
    },


    //get All Waves By UserID and Contact Name
    // getAllWaves: (req, res) => {
    //     const { userID,email } = req.body;
    //     Contact.findOne({ user: userID,email:email }, { 'waves': 1, '_id': 0 }).populate('waves')
    //         .then((waves) => {
    //             console.log(waves)
    //             res.status(200).json({
    //                 waves
    //             }
    //             )
    //         }).catch(error => {
    //             res.status(500).json({
    //                 error
    //             })
    //         })
    // },

    //need to move to the Master
    //reply from leaderBox
    //emails= wave.to
    //must get conversationID in the wave
    newWave: async (req, res) => {
        console.log("in new waveeee!")
        const { wave } = req.body;
        const Cuser = await User.findOne({ username: req.params.username }).catch((err) => { res.status(500).json({ err }) });
        const userID = Cuser._id;
        const jwt = req.headers.authentication;
        let sendEmail = {};
        var newConversation = {}
        var rootConversationID = '';
        wave.from = Cuser.username;
        var newWave = new Wave(wave);
        Conversation.findByIdAndUpdate(newWave.conversation, { $push: { waves: newWave._id }, })
            .then(async (conversation) => {
                console.log("arrived to findByIdAndUpdate newWave then")
                newConversation = conversation;
                newWave.conversation = newConversation._id;
                await checkContactIDInSpam(Cuser, newWave, newConversation)
                newWave.save().then(async (result) => {
                    if (conversation.source == 'chat') {
                        console.log(userID, chatId);
                        countLeads(userID, chatId)
                            .then((response, error) => {
                                console.log("response", response);
                                return res.status(200).json({ message: 'success', response })
                            })
                    }
                    else if (conversation.source != 'Leader Box' && conversation.source != 'Box') {
                        console.log("save wave!!!!!!!!!");
                        return res.status(200).json({ message: 'success', newWave })
                    }
                    else {
                        console.log("result", conversation);
                        rootConversationID = newConversation.rootConversationID;
                        console.log("rootConversationID1", rootConversationID);
                        console.log("newWave!!", newWave);
                        sendEmail.html = newWave.body + "<p>aaaaaaaa</p>" + "<img src='https://mailart.leader.codes/routerMails/tracker/'" + wave.from + " >;"
                        sendEmail.subject = newConversation.subject;
                        sendEmail.to = newWave.to;
                        sendEmail.from = newWave.from;
                        //for meanwile- till the mails will work good
                        //   sendMessage(sendEmail, jwt)
                        newWave.to.forEach(async email => {
                            let newContact
                            //if the 'email' is groupName = saving to each contact in the group
                            let myGroup = null
                            if (Cuser.groups.length !== 0 && Cuser.groups[0] !== null) {
                                myGroup = await Cuser.groups.find(g => g.groupName === email)
                            }
                            if (myGroup) {
                                // await newWave.toId.push(myGroup._id)
                                await myGroup.members.forEach(async (member) => {
                                    newContact = await Contact.findOne({ _id: member })
                                })
                            }
                            else {
                                newContact = await Contact.findOne({ user: userID, email: email });
                                //if new contact
                                if (!newContact) {
                                    // console.log("new contact created to user");
                                    // newContact = new Contact()
                                    // newContact.user = userID;
                                    // newContact.email = email;
                                    // newContact.name = email.substring(0, email.indexOf('@'))

                                    //add contat to user's contacts
                                    const body =
                                    {
                                        "contact": {
                                            "email": email,
                                        },
                                        "withConversation": false,
                                        "source": {
                                            "type": newConversation.source
                                        }
                                    }
                                    let tempContact = await createNewContact(body, jwt, req.params.username)
                                    // newWave.toId.push(tempContact.newContact._id)
                                    console.log("contactttt", newContact)

                                }
                            }
                            if (email.split('@mails.codes')[0] === req.params.username) {
                                // update conversation to unreaded;   
                                newConversation.readed = false;
                            }
                            else {
                                saveWaveToReciver(newContact, newWave, Cuser, rootConversationID, newConversation, jwt)
                            }
                        })
                        await newConversation.save().then(async (result) => {
                            Cuser.save().then((result2) => {
                                console.log("save newWaveeeee: ", newWave)
                                res.status(200).json({ message: "new wave saved", newWave: newWave })
                            }).catch((err) => {
                                res.status(500).json({ err, message: 'newConversation.save()' })
                            })
                            // console.log("newConversation", result);
                            //   var newConversation2 = await Conversation.findById({ _id: newConversation._id }).populate('contacts').populate('waves');
                        }).catch((err) => {
                            res.status(500).json({ err, message: 'newConversation.save()' })
                        })
                    }
                }).catch((err) => {
                    res.status(500).json({ err, message: 'newWave.save()' })

                })

            }).catch((err) => {
                res.status(500).json({ err, message: 'newConversation.waves.push(newWave._id)' })
            })

    },


    //get last wave by conversation ID
    getLastWave: async (req, res) => {
        //  const Cuser = await User.findOne({ username: req.params.username });
        //  const userID = Cuser._id;
        const { conversationId } = req.body;
        Conversation.findById(conversationId).populate('waves').then((con) => {
            console.log(con.waves.length);
            var length = con.waves.length;
            var lastWave = con.waves[length - 1];
            res.status(200).json({ lastWave })
        }).catch((err) => {
            res.status(500).json({ err, message: "cant find conversation" })
        })
    },
    uploadFiles: async (req, res) => {
        console.log("in uploadFiles . req.files= ", req.files)
        const Cuser = await User.findOne({ username: req.params.username });
        let files = await uploadedMultipleFiles(req.files, req.params.username, req.headers["authorization"])//upload img
        console.log("data upload succeddded!!!", files);
        res.status(200).send({ files });
    },

    downloadFile: async (req, res) => {
        console.log(req.body);
        console.log("in dddddddddddownload");
        // var file;
        // let f = req.body.replace("https://files.codes/uploads", "");
        // console.log(f);
        // file = path.join(__dirname, "../uploads/", f);//download by name
        // res.download(file);
    },


    removeFile: async (req, res) => {
        const Cuser = await User.findOne({ username: req.params.username })
        // console.log("lklkl", req.files);
        //const Cuser = await User.findOne({ uid: req.params.uid }).catch((err) => { res.status(500).json({ err }) });
        //  const userID = Cuser._id;
        //console.log("lklklk",req.files);
        const { file } = req.body;
        const jwt = req.headers.authentication;
        await request.get("https://files.leader.codes/api/" + Cuser.uid + "/remove/" + file + "/", {
            headers: { Authorization: jwt }
        }, (error, res, body) => {
            if (body)
                console.log("arrive to -body");
            if (res)
                console.log("arrive to -res", res);
            if (error) {
                console.log("arrive to -err");

                console.error(error)
                return
            }
        })
        res.status(200).json({ message: "success" })


    },

    recognizedEmailOpened: (req, res) => {
        const email = req.params.email;
        console.log("recognized Email Opened  ", email + " open email");
    }


}
uploadedMultipleFiles = (fileToUpload, userName, headers) => {
    return new Promise(async (resolve, reject) => {
        console.log("in uploadedMultipleFiles", fileToUpload)
        const uri = `https://files.codes/api/${userName}/uploadMultipleFiles`;
        console.log("files:", Object.keys(fileToUpload));
        let formdata = {}
        Object.keys(fileToUpload).forEach(file => {
            let currentFile =
            {
                [file]: {
                    value: fileToUpload[file].data,
                    options: {
                        filename: fileToUpload[file].name
                    },
                }
            }

            formdata = Object.assign(currentFile, formdata);
        });
        console.log("formdata", formdata);
        const options = {
            method: "POST",
            url: uri,
            headers: {
                Authorization: headers,
                "Content-Type": "multipart/form-data",
            },
            formData: formdata
        }
        request(options, async (err, res, body) => {
            if (err) {
                console.log(err);
                reject(err);
            }
            console.log("result from server", body);
            try {
                const files = JSON.parse(body).filesData;
                resolve(files);
            } catch (error) {
                reject(error);
            }
        });
    });
    // });
}


function sendMessage(email, jwt) {
    console.log("email!!", email);
    console.log("jwt!!", jwt);
    //  email.from= 'chavilax@gmail.com'
    console.log("arrive to sendEmail");

    request.post('https://api.leader.codes/mail/sendEmail', {
        json: email,
        headers: { Authorization: jwt }
    }, (error, res, body) => {
        console.log("arrive to sendEmail", body);

        if (error) {
            console.error(error)
            return
        }

    })

}


//contact1- contact
// contact- user
countLeads = (userID, chatId) => {
    console.log(userID, chatId);
    return new Promise((resolve, reject) => {
        request.post(`https://chat.leader.codes/api/${userID}/countLeads/${chatId}`, {
        }, (error, res, body) => {
            console.log("arrive to chatID", body);
            resolve("success!!")
            if (error) {
                console.log(error)
                reject(error)
            }

        })
    })
}

//need to move to the Master
//conversation: to create new conversation to new reciver
//(if the reciver dont have this conversation already)
async function saveWaveToReciver(contact1, wave, userContact, rootConversationID, conversation, jwt) {
    console.log("arrive to saveConversationToReciver contact1=", contact1.email, contact1.username);
    console.log("arrive to saveConversationToReciver userContact=", userContact.email, userContact.username);

    var newConversation = {}
    var newWave = {}
    let userNameVM = ''
    var user

    if (contact1.email.includes('@mails.codes'))
        userNameVM = await contact1.email.split('@mails.codes')[0]
    if (userNameVM !== '') {
        user = await User.findOne({ username: userNameVM });
        console.log("userrrrrrrrrrrr", user)
    }
    //remove the option to send to contact in box with his assign mail
    // else
    //     user = await User.findOne({ email: contact1.email });

    if (!user)
        return;
    console.log("contact exist as userrr");
    console.log("rootConversationID!", rootConversationID);

    Conversation.findOne({ user: user._id, rootConversationID: rootConversationID }).then(async (result) => {
        if (result == null) {
            console.log("arrive to null");
            newConversation = new Conversation();
            newConversation.user = user._id;
            user.conversations.push(newConversation._id);
            newConversation.source = conversation.source;
            newConversation.rootConversationID = conversation.rootConversationID;
            newConversation.subject = conversation.subject;
        }
        else {
            newConversation = result;
        }
        newWave = new Wave(wave);
        newConversation.waves.push(newWave._id);
        newConversation.readed = false;
        newWave.conversation = newConversation._id;
        newWave.accepted = true;
        var newContact = await Contact.findOne({ user: user._id, email: userContact.email })
        if (!newContact) {
            console.log("user is now new contact to contact!!");
            newContact = new Contact()
            newContact.user = user._id;
            newContact.email = userContact.email;
            newContact.name = userContact.email.substring(0, userContact.email.indexOf('@'))
            //add contat to user's contacts
            user.contacts.push(newContact._id)
        }
        newConversation.contacts.push(newContact._id);
        newContact.conversations.push(newConversation._id)
        newContact.save().then((result) => {
            user.save().then((result) => {
                newConversation.save().then((result) => {
                    console.log("conversation", result);
                    newWave.save().then((result) => {
                    }).catch((err) => {
                        err
                    })
                }).catch((err) => {
                    err
                })
            }).catch((err) => {
                err
            })
        }).catch((err) => {
            err
        })


    })
}


async function checkContactIDInSpam(user, wave, conversation) {
    console.log('check Contact ID In Spam!!!!!!!');
    let contactName = wave.from
    // let contactName = wave.from || wave.fromId


    console.log(contactName)
    try {
        let Ccontact = await Contact.findOne({ name: contactName })
        console.log(Ccontact.name)
        const contactID = Ccontact && Ccontact._id
        console.log(contactID)
        let a = contactID && user && user.spamContactID && user.spamContactID.find(x => x !== contactID)
        if (a) {
            console.log(conversation.spam)
            conversation.spam = true
            await conversation.save()
            console.log(conversation.spam)
        }
        console.log('finish check Contact ID In Spam!!!!!!!');
    }
    catch {
        return
    }

}

function createNewContact(contact, jwt, userName) {
    console.log("in createNewContact ", contact, userName);
    const options = {
        url: `https://api.dev.leader.codes/${userName}/createContact`,
        headers: { Authorization: jwt },
        method: "POST",
        json: contact,
    };
    return new Promise(async (resolve, reject) => {
        request(options, (error, res, body) => {
            if (error) {
                console.error(error);
                reject(error);
            }
            if (res && !res.statusCode == 200) reject(body);
            console.log(`statusCode: ${res.statusCode}`);
            console.log(body);
            resolve(body);
        });
    });
}



