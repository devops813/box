const router = require('express').Router()

const { saveFcmToUser, getUsersEmails, getAllContacts, deleteContact, newContact, editContact, getUidByUserName, getUser } = require('../controller/contact')
const { getConversationBySubject, saveConversationGlobal, getConversationFromContact, getAllConversations, newConversation, deleteConversations, deleteConversation, deleteSystemWaves, archiveConversations, editConversation, editConversations, newSource, getAllSources, sendConversationByEmail, createSystemWave, markAsReadOrUnreadConversations,markAsTrashOrUntrashConversations } = require('../controller/conversation')
const { defaultTags, deleteTag, deleteTags, editTag, editTags, newTag, getAllTags, newGmailTag } = require('../controller/tag')
const { getLastWave, removeFile, deleteWave, newWave, getAllWaves, uploadFiles, getSystemWaves, downloadFile, recognizedEmailOpened } = require('../controller/wave')
const { newSignature, getAllSignatures, uploadPhoto, deleteSignature, editSignature } = require('../controller/signature')
const { exportSignature } = require('../controller/exportSignature')
const { newGroup, deleteGroup, getGroups, deleteAllGroups, getGroupByName, editGroup } = require('../controller/group')
const { getDrafts, addDraft, editDraft, deleteDraft } = require('../controller/draft')
const { getGmailAddress, gmailMassage, ifLabelsGmail, changeEnableGoogleToken, sendGmail } = require('../controller/google')
const { addNewSource, deleteSource, getSources, editSource } = require('../controller/source')
const { getAllSpamContactsIDs, addSpamContactID, deleteSpamContactID } = require('../controller/spam')
const { newSubUser, getUsersByManager, deleteSubUser } = require('../controller/subUsers')
const { checkPermission, checkSourseOfUser } = require('../controller/checkPermission')
const { getAll, createCopyContent, deleteCopyContent, createClipboardSystemWave } = require('../controller/copyContent')
const { createTabHistory, getAllTabHistoryByUsername, updateTabHistory, tabToHistory } = require('../controller/tabsHistory')
const { createNote, getNotesByUserName, updateNote, updateNoteByID, deleteNotesByUserName, deleteNoteByMongoId } = require('../controller/note')
const {
  addTokenToNotification,
  checkIfTokenIsExsist,
  removeTokenFromNotification,
  createNotification,
  addUserToNotification,
  removeUserFromNotification,
  getAllNotaficaionsToManager,
  deleteNotafication,
  sendNotification,
  sendNotaficationToAll
} = require('../controller/notification')

//Contact
router.get('/:username/contact/getUsersEmails', getUsersEmails)
router.get('/getUidByUserName/:userName', getUidByUserName)
router.get('/getUser/:userName', getUser)
router.post('/:username/saveFcmToUser', saveFcmToUser)
router.post('/:username/contact/newContact', newContact)
router.post('/:username/contact/editContact', editContact)
router.post('/:username/contact/getAllContacts', getAllContacts)
router.delete('/:username/contact/deleteContact', deleteContact)

//conversation
router.post('/:username/conversation/getConversationBySubject', getConversationBySubject)
router.post('/:username/conversation/getConversation', getConversationFromContact)
router.post('/:username/conversation/saveConversationGlobal', saveConversationGlobal)
router.post('/:username/conversation/getAllSources', getAllSources)
router.post('/:username/conversation/archiveConversations', archiveConversations)
router.post('/:username/conversation/newSource', newSource)
router.post('/:username/conversation/editConversation', editConversation)
router.post('/:username/conversation/editConversations', editConversations)
router.post('/:username/conversation/markAsReadOrUnreadConversations', markAsReadOrUnreadConversations)
router.post('/:username/conversation/getAllConversations', getAllConversations)
router.post('/:username/conversation/newConversation', newConversation)
router.post('/:username/conversation/sendConversationByEmail', sendConversationByEmail)
router.post('/createSystemWave', createSystemWave)
router.post('/:username/conversation/markAsTrashOrUntrashConversations', markAsTrashOrUntrashConversations)
router.delete('/:username/conversation/deleteConversations', deleteConversations)
router.delete('/:username/conversation/deleteSystemWaves', deleteSystemWaves)
router.delete('/:username/conversation/deleteConversation', deleteConversation)

//Tag
router.post('/:username/tag/newTag', newTag)
router.post('/:username/tag/defaultTags', defaultTags)
router.post('/:username/tag/editTag', editTag)
router.post('/:username/tag/editTags', editTags)
router.post('/:username/tag/getAllTags', getAllTags)
router.post('/:username/tag/newGmailTag', newGmailTag)
router.delete('/:username/tag/deleteTags', deleteTags)
router.delete('/:username/tag/deleteTag', deleteTag)

//wave
router.get('/tracker/:email', recognizedEmailOpened);
router.post('/:username/wave/getLastWave', getLastWave)
router.post('/:username/wave/removeFile', removeFile)
router.post('/:username/wave/uploadFiles', uploadFiles)
router.post('/:username/wave/downloadFile', downloadFile)
router.post('/:username/wave/newWave', newWave)
router.post('/:username/wave/getSystemWaves', getSystemWaves)

//Signature
router.post('/:username/signature/newSignature', newSignature)
router.post('/:username/signature/getAllSignatures', getAllSignatures)
router.post('/:username/signature/editSignature', editSignature)
router.post('/:username/signature/deleteSignature', deleteSignature)
router.post('/:username/signature/uploadPhoto/:uId', uploadPhoto)
router.post('/:username/signature/exportSignatureToGmail', exportSignature)

//Group
router.post('/:username/group/newGroup', newGroup)
router.post('/:username/group/deleteGroup', deleteGroup)
router.post('/:username/group/getGroups', getGroups)
router.post('/:username/group/deleteAllGroups', deleteAllGroups)
router.post('/:username/group/getGroupByName', getGroupByName)
router.post('/:username/group/editGroup', editGroup)

//Draft
router.post('/:username/draft/getDrafts', getDrafts)
router.post('/:username/draft/addDraft', addDraft)
router.post('/:username/draft/editDraft', editDraft)
router.delete('/:username/draft/deleteDraft', deleteDraft)

//google
// router.get('/:userName/google/getGmailAddress', getGmailAddress)
// router.post('/:userName/google/gmailMassage', gmailMassage)
// router.get('/:userName/google/ifLabelsGmail', ifLabelsGmail)
// router.post('/:userName/google/sendGmail', sendGmail)
// router.post('/:userName/changeEnableGoogleToken', changeEnableGoogleToken)

//source
router.post('/:userName/source/newSource', addNewSource)
router.post('/:userName/source/deleteSource', deleteSource)
router.post('/:userName/source/getSources', getSources)
router.post('/:userName/source/editSource', editSource)

//spam
router.get('/:userName/spam/getAllSpamContactsIDs', getAllSpamContactsIDs)
router.post('/:userName/spam/addSpamContactID', addSpamContactID)
router.delete('/:userName/spam/deleteSpamContactID', deleteSpamContactID)

///checkSourseOfUser
router.get('/:userName/checkSourseOfUser', checkSourseOfUser)

//notification
router.post('/:userName/notification/addTokenToNotification', addTokenToNotification)
router.post('/:userName/notification/checkIfTokenIsExsist', checkIfTokenIsExsist)
router.post('/:userName/notification/removeTokenToNotification', removeTokenFromNotification)
router.post('/:userName/notification/createNotification', createNotification)
router.post('/:userName/notification/newUserToNotification', addUserToNotification)
router.post('/:userName/notification/getAllNotaficaionsToManager', getAllNotaficaionsToManager)
router.post('/:userName/notification/deleteNotafication', deleteNotafication)
router.post('/:userName/notification/sendNotification', sendNotification)
router.post('/:userName/notification/sendNotaficationToAll', sendNotaficationToAll)
router.delete('/:userName/notification/removeUserFromNotification', removeUserFromNotification)

//copyContent
router.get('/:userName/extensions/getClipboard', getAll);
router.post('/:userName/extensions/createClipboardSystemWave', createClipboardSystemWave);
router.post('/:userName/extensions/createCopyContent', createCopyContent);
router.delete('/:userName/extensions/:id/deleteCopyContent', deleteCopyContent)

//history
router.get('/:userName/extensions/getAllTabHistoryByUsername', getAllTabHistoryByUsername);
router.post('/:userName/extensions/createTabHistory', createTabHistory);
router.post('/:userName/extensions/:tabId/updateTabHistory', updateTabHistory)
router.post('/:userName/extensions/:tabId/tabToHistory', tabToHistory)

//notes
router.get('/:username/note/getNotesByUserName', getNotesByUserName)
router.post('/:username/note/createNote', createNote);
router.post('/:username/note/:indexNote/updateNote', updateNote)
router.post('/:username/note/:indexNote/updateNoteByID', updateNoteByID)
router.delete('/:username/note/:indexNote/deleteNote', deleteNotesByUserName)
router.delete('/:_id/note/deleteNote2', deleteNoteByMongoId)

//subUser
router.get('/getUsersByManager/:userManager', getUsersByManager)
router.post('/:userName/subUsers/newSubUser', newSubUser)
router.post('/:userName/subUsers/deleteSubUser', deleteSubUser)
// router.get('/:userName/isPermission',isPermission)
module.exports = router