const mongoose = require('mongoose')

const ticketSchema = new mongoose.Schema({

    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    contact: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Contact'
    },
    tNumber:
        { type: String }
    ,
    subject:
        { type: String }
    ,
    status:
        { type: Number }
    ,
    department:
        { type: mongoose.Schema.Types.ObjectId, ref: 'Departments' }
    ,
    wave: [
        { type: mongoose.Schema.Types.ObjectId, ref: 'Wave' }
    ],
})
ticketSchema.index({ subject: 'text' })

module.exports = mongoose.model('Ticket', ticketSchema)

