// const commonmodels = require('@leadercodes/modelsnpm');
// const User = commonmodels.models.user
const request = require('request');
const User = require('../models/User')

module.exports = {
  checkPermission: async (req, res, next) => {
    console.log("in checkPermission !!!!!!!!!!!!!!");
    const host = req.get('host');
    const isLocal = (req.query.isLocal == 'true');
    console.log("newIsLocal", isLocal);
    let redirectUrl1 = host + "/admin";

    if (isLocal)
      return next();
    console.log("in checkPermission", req.originalUrl.split("/"));
    let userName = req.originalUrl.split("/")[1];
    let apiFlag = false
    let urlRoute
    let redirectUrl = host + "/admin";
    if (userName == "api") {
      userName = req.originalUrl.split("/")[2];
      console.log("name@@",userName)
      apiFlag = true
    }
    if (!apiFlag) urlRoute = req.originalUrl.split("/")[3]
    if (!userName) {
      console.log("no uid");
      return res.status(401).json({ des: redirectUrl, routes: urlRoute, apiFlag: apiFlag, status: 401 })
    }
    else {
      if(userName==="notification")
        return next()
      console.log("in else !!!");
      const jwt = req.cookies.devJwt ? req.cookies.devJwt : req.headers['authorization'] ? req.headers['authorization'] : null
      const cookie = request.cookie(`jwt=${jwt}`)
      const options = {
        method: "GET",
        url: `https://dev.accounts.codes/isPermission/${userName}`,
        headers: { Cookie: cookie }
      };
      request(options, (error, response, body) => {
        console.log("response.statusCode", response.statusCode)
        if (error || response.statusCode != 200) {
          console.log("@@error")
          return res.status(401).json({ des: redirectUrl, routes: urlRoute, apiFlag: apiFlag, status: 401 })
        }
        else {
          if (body == 'true') {
            console.log("no error!!!!!!!");
            return next();
          }
          return res.status(401).json({ des: redirectUrl, routes: urlRoute, apiFlag: apiFlag, status: 401 })
        }
      });
    }
  },
  checkSourseOfUser: (req, res, next) => {
    console.log('check Source Of User !!!!!!!!!!');
    const sourse = req.useragent;
    if (sourse.isMobile || sourse.isMobileNative || sourse.isTablet || sourse.isiPad || sourse.isiPod  || sourse.isAndroid || sourse.isAndroidNative)
    // || sourse.isiPhone || sourse.isiPhoneNative
      console.log("is Mobile !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
      else console.log('no mobile ');
    res.send(sourse.isMobile);
  },
  isPremission: async(req,res)=>{
    console.log("arrive@@@@@")
    let userName = req.params.userName
    if (userName == "api") {
      userName = req.originalUrl.split("/")[2];
    }
  
    let currentUser = await User.findOne({ username: userName })
    if (!currentUser) {
      let newUser = new User();
      const  jwt = req.cookies.devJwt ? req.cookies.devJwt : req.headers['authorization'] ? req.headers['authorization'] : null
      const cookie = request.cookie(`jwt=${jwt}`)  
      const options = {
        method: "GET",
        url: `https://dev.accounts.codes/api/${userName}`,
        headers: { Cookie: cookie }
      };
      request(options, async (error, response, body) => {
        console.log("response.statusCode", response.statusCode)
        console.log("tryy jwt", cookie);
  
        if (error || response.statusCode != 200) {
          console.log("url____________ ", redirectUrl);
          return res.status(401).json({ des: redirectUrl, routes: urlRoute, apiFlag: apiFlag, status: 401 })
        }
        else {
          newUser.username = userName;
          newUser.email = JSON.parse(body).user.email
          await newUser.save().then((result) => {
            console.log("save user");
            res.status(200).json({ res: result })
          })
            .catch(error => {
              console.log("error", error);
              res.status(500).json({
                error: error
              })
            })
        }
  
      });
    }
    else{
      res.status(200).send()
    }
  }
 
}