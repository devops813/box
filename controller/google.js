const request = require('request');

// const commonmodels = require('@leadercodes/modelsnpm');
// const Tag = commonmodels.models.tag
// const User = commonmodels.models.user


const Tag = require('../models/Tag')
const User = require('../models/User')

module.exports = {
  sendGmail: async (req, res) => {
    console.log("in send gmail");
    const options = {
      url: `https://api.leader.codes/sendGmail`,
      headers: { Authorization: req.body.accessToken },
      method: "POST",
      json: req.body.data,
  };
  return new Promise(async (resolve, reject) => {
      request(options, (error, res, body) => {
          if (error) {
              console.error(error);
              reject(error);
          }
          if (res && !res.statusCode == 200) reject(body);
          console.log(`after send mail to gmail statusCode: ${res.statusCode}`);
          console.log(body);
          resolve(body);
      });
  });
   },


 ifLabelsGmail: async (req, res) => {
   console.log("ifLaelsGmail",req.params.userName )
   let fleg=false
   const UserTags = await User.findOne({ username: req.params.userName }).populate('tags');
   if(!UserTags)
      return res.status(500).send("user invalid")

   const g =  UserTags.tags.map(a=>
   {
     if(a.source=='google')
     fleg=true;
   })
   return res.status(200).send(fleg)

 },
    getGmailAddress: async (req, res) => {
     const options = {
    method: 'GET',
    url: `https://api.dev.leader.codes/getUserEmailSignIn`,
    headers: { Authorization: req.headers['authorization'] }
  }
   await request(options, (error, response, body) => {
      console.log("res",response.statusCode, body)
    if (error || response.statusCode != 200)
    res.status(500).send("erroe")
    res.status(200).send(body)

  })

    },
    //24 שעות
    gmailMassage: async (req, res) => {
    console.log("#gmail", req.headers['authorization'])
    const options = {
    method: 'POST',
    url: `https://api.dev.leader.codes/gmailListMessages`,
    headers: { Authorization: req.headers['authorization'] }
  }
   await request(options, (error, response, body) => {
      console.log("res",response.statusCode)
    if (error || response.statusCode != 200)
    res.status(500).send("erroe")
    let bodyJson=JSON.parse(body);
    res.status(200).send(bodyJson)

  })
    },
    //changeEnableGoogleToken
 changeEnableGoogleToken:async (req, res)=>{
  let user = await User.findOne({username: req.params.userName})
  user.googleTokens[0].enable=!user.googleTokens[0].enable
  user.save();
  console.log( 'userGoogleToken: ', user.googleTokens[0].enable)
  res.status(200).json({ userGoogleToken:user.googleTokens[0].enable }) 
 }
}