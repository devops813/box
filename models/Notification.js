const mongoose = require('mongoose')
const notificationSchema = new mongoose.Schema({
    code: String,
    name: String,
    manager: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    usersList: [
        { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
    ],
})

module.exports = mongoose.model('Notification', notificationSchema)
