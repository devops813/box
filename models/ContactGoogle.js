const mongoose = require('mongoose')

const ContactGoogle = new mongoose.Schema({ 
   userEmailSignIn:{type:String},
   contact:{ type: mongoose.Schema.Types.ObjectId, ref: 'Contact' },
   resourceName: {type:String},
  etag: {type:String},
  metadata: {
   type: mongoose.
   Schema.Types.Mixed
  },
  addresses: [
    {
     type: mongoose.Schema.Types.Mixed
    }
  ],  
  ageRanges: [
    {
    type: mongoose.Schema.Types.Mixed
    }
  ],
  biographies: [
    {
    type: mongoose.Schema.Types.Mixed
    }
  ],
  birthdays: [
    {
      type: mongoose.Schema.Types.Mixed
    }
  ],
  braggingRights: [
    {
        type: mongoose.Schema.Types.Mixed
    }
  ],
  calendarUrls: [
    {
     type: mongoose.Schema.Types.Mixed
    }
  ],
  clientData: [
    {
     type: mongoose.Schema.Types.Mixed
    }
  ],
  coverPhotos: [
    {
    type: mongoose.Schema.Types.Mixed
    }
  ],
  emailAddresses: [
    {
        type: mongoose.Schema.Types.Mixed
    }
  ],
  events: [
    {
    type: mongoose.Schema.Types.Mixed
    }
  ],
  externalIds: [
    {
      type: mongoose.Schema.Types.Mixed
    }
  ],
  fileAses: [
    {type: mongoose.Schema.Types.Mixed
    }
  ],
  genders: [
    {
    type: mongoose.Schema.Types.Mixed
    }
  ],
  imClients: [
    {
        type: mongoose.Schema.Types.Mixed
    }
  ],
  interests: [
    {
     type: mongoose.Schema.Types.Mixed
    }
  ],
  locales: [
    {
   type: mongoose.Schema.Types.Mixed
    }
  ],
  locations: [
    {
     type: mongoose.Schema.Types.Mixed
    }
  ],
  memberships: [
    {type: mongoose.Schema.Types.Mixed
    }
  ],
  miscKeywords: [
    {
     type: mongoose.Schema.Types.Mixed
    }
  ],
  names: [
    {
  type: {type: mongoose.Schema.Types.Mixed}
    }
  ],
  nicknames: [
    {
     type: mongoose.Schema.Types.Mixed
    }
  ],
  occupations: [
    {
      type: mongoose.Schema.Types.Mixed
    }
  ],
  organizations: [
    {
    type: mongoose.Schema.Types.Mixed
    }
  ],
  phoneNumbers: [
    {
     type: mongoose.Schema.Types.Mixed
    }
  ],
  photos: [
    {
      type: mongoose.Schema.Types.Mixed
    }
  ],
  relations: [
    {
      type: mongoose.Schema.Types.Mixed
    }
  ],
  relationshipInterests: [
    {
     type: mongoose.Schema.Types.Mixed
    }
  ],
  relationshipStatuses: [
    {
      type: mongoose.Schema.Types.Mixed
    }
  ],
  residences: [
    {
    type: mongoose.Schema.Types.Mixed
    }
  ],
  sipAddresses: [
    {
      type: mongoose.Schema.Types.Mixed
    }
  ],
  skills: [
    {
      type: mongoose.Schema.Types.Mixed
    }
  ],
  taglines: [
    {
      type: mongoose.Schema.Types.Mixed
    }
  ],
  urls: [
    {
     type: mongoose.Schema.Types.Mixed
    }
  ],
  userDefined: [
    {
     type: mongoose.Schema.Types.Mixed
    }
  ]

})


module.exports = mongoose.model('ContactGoogle', ContactGoogle)