// const commonmodels = require('@leadercodes/modelsnpm');
// const Contact = commonmodels.models.contact
// const User= commonmodels.models.user

const Contact = require('../models/Contact');
const User = require('../models/User.js');
const request = require('request');
//
module.exports = {

    getUidByUserName: async (req, res) => {
        console.log("inside!!")
        const userName = req.params.userName
        //console.log(userName)
        const user = await User.findOne({ username: userName })
        if (user)
            res.json({ "uid": user.uid })
    },

    getUser: async (req, res) => {
        const userName = req.params.userName
        const user = await User.findOne({ username: userName })
        if (user)
            res.json({ user })
    },

    saveFcmToUser: async (req, res) => {
        const { fcmToken } = req.body;
        console.log("fcm", fcmToken);

        User.findOneAndUpdate({ username: req.params.username }, { fcmToken: fcmToken }).then((user) => {
            res.status(200).json({ user })
        }).catch(error => {
            res.status(500).json({
                error: error
            })
        })

    },
    getAllContacts: async (req, res) => {
        console.log("in getAllContacts");
        const Cuser = await User.findOne({ username: req.params.username });
        const userID = Cuser._id;

        await Contact.find({ user: userID })
            .then((contacts) => {
                res.status(200).json({
                    contacts
                })
            }).catch(error => {
                console.log(error)
                res.status(500).json({
                    error: error
                })
            })
    },

    getUsersEmails: async (req, res) => {
        User.find().select("email username")
            .then((users) => {
                res.status(200).json({
                    users
                })
            }).catch(error => {
                console.log(error)
                res.status(500).json({
                    error: error
                })
            })
    },
    ///exist in master
    newContact: async (req, res) => {
        console.log("newContact ");
        const { contact, source } = req.body;
        console.log("body", contact, source);
        const newContact = new Contact(contact);
        const Cuser = await User.findOne({ username: req.params.username });
        const userID = Cuser._id;

        //('cant save contact with no email')
        if (!contact.email) {
            return res.status(500).json({
                message: 'cant save contact with no email'
            })
        }
        //check if the contact exist already
        const existContact = await Contact.findOne({ user: userID, email: contact.email });
        if (existContact) {
            var source1 = false;
            //check the contact have this source in source array
            if (source) {
                if (source.id != null && source.type != null) {
                    existContact.source.forEach(element => {
                        if (element.id == source.id && element.type == source.type)
                            source1 = true;
                    });
                }
                else if (source.type != null) {
                    existContact.source.forEach(element => {
                        if (element.type == source.type)
                            source1 = true;
                    });
                }
                else if (source.id != null) {
                    existContact.source.forEach(element => {
                        if (element.id == source.id)
                            source1 = true;
                    });
                }
                if (source1 == false) {
                    existContact.psourceush(source)
                    existContact.save().then((result) => {

                    }).catch((err) => {
                        return res.status(500).json({
                            err
                        })

                    })

                }


            }//if (source)

            return res.status(200).json({
                existContact,
                message: 'contact exist already'
            })
        }//if exist contact
        const email = Cuser.email
        applicationName = 'Leader';
        const jwt = req.headers.authentication;
        await request.post('https://pay.leader.codes/premium', {
            json: { email, applicationName },
            headers: { Authorization: jwt }
        }, (error, res, body) => {
            if (!body.planIndex) {
                if (Cuser.contacts.length >= 5000)
                    newContact.displayToUser = false;
            }
            if (error) {
                console.error(error)
            }
        })

        newContact.user = userID;
        if (source) {
            newContact.source.push(source);
        }
        newContact.save(contact)
            .then(async (result) => {
                // console.log("QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ");

                await request.post('https://hooks.zapier.com/hooks/catch/8846183/oe8m43t/', {
                    json: { contact: newContact },
                    //headers: { Authorization: jwt }
                    headers: { API_key: 'abcd' }
                }, (error, res, body) => {
                    if (body)
                        console.log("arrive to zapier-body", body);
                    if (res)
                        console.log("arrive to zapier-res", res);
                    if (error) {
                        console.log("arrive to zapier-err", error);

                        console.error(error)
                        return
                    }
                })
                console.log("res", result);

                User.updateOne({ _id: userID }, { $push: { contacts: result._id } }).then((result2) => {
                    console.log("contact created")
                    return res.status(200).json({
                        result,
                        message: 'contact created'
                    })
                }).catch(error => {
                    return res.status(500).json({
                        error,
                        message: 'cant add contact to user'
                    })
                })

            }).catch(error => {
                return res.status(500).json({
                    error,
                    message: 'cant save contact'

                })
            })

    },

    //exist in the Master
    editContact: (req, res) => {
        const { contact } = req.body;
        Contact.findById(contact._id).then((result) => {
            result.set(contact);
            result.save();
            res.status(200).json({
                message: 'contact updated',
                result
            })
        }).catch(error => {
            res.status(500).json({
                error
            })
        })
    },

    // move to the Master
    deleteContact: (req, res) => {
        const { contactID } = req.body;
        Contact.findByIdAndDelete(contactID).then((result) => {

            User.updateOne({ _id: result.user }, { $pull: { contacts: contactID } }).then((result2) => {

                res.status(200).json({
                    message: 'contact deleted'
                })
            })
        }).catch(error => {
            res.status(500).json({
                error
            })
        })
    },


}