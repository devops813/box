const mongoose = require('mongoose')
const userSchema = new mongoose.Schema({
    email: { type: String, require: true, unique: true, match: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ },
    uid: { type: String, require: true },
    premium: { type: Boolean },
    contacts: [
        { type: mongoose.Schema.Types.ObjectId, ref: 'Contact' }
    ],
    conversations: [
        { type: mongoose.Schema.Types.ObjectId, ref: 'Conversation' }
    ],
    // notifications: [
    //     { type: mongoose.Schema.Types.ObjectId, ref: 'Notification' }
    // ],
    notification: [{
        token: { type: String },
        joiningDate: { type: Date },
    }],
    videos: [
        { type: mongoose.Schema.Types.ObjectId, ref: 'Video' }
    ],
    tags: [
        { type: mongoose.Schema.Types.ObjectId, ref: 'Tag' }
    ],
    landingPages: [
        { type: mongoose.Schema.Types.ObjectId, ref: 'LandingPage' }
    ],
    forms: [
        { type: mongoose.Schema.Types.ObjectId, ref: 'Form' }
    ],
    cards: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Card'
    }],
    events: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Event'
    }],
    blog: { type: mongoose.Schema.Types.ObjectId, ref: 'Blog' },
    chats: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Chat'
    }],
    tickets: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Ticket'
    }],
    systemWaves: [{ type: mongoose.Schema.Types.ObjectId, ref: 'SystemWave' }],
    username: String,
    fcmToken: String,
    signatures: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Signature' }],
    // groups: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Group' }]
    groups: {
        type: [{
            groupName: { type: String},
            members: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Contact' }]
        }], default: []
    },
    drafts: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Draft'
    }],

    googleTokens: [{
        googleToken: { type: String },
        enable: { type: Boolean, default: true }
    }],
    spamContactID: [{ type: mongoose.Schema.Types.ObjectId }],
    notes: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Notes' }],
    copiesContents: [{ type: mongoose.Schema.Types.ObjectId, ref: 'CopyContent' }],
    tabsHistory: [{ type: mongoose.Schema.Types.ObjectId, ref: 'TabHistory' }],
    userType: { type: String }, //manager or subUser
    userManager: { type: String } // userName of the manager
})

module.exports = mongoose.model('User', userSchema)