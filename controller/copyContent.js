// const commonmodels = require('@leadercodes/modelsnpm');
// const CopyContent = commonmodels.models.copyContent
// const User = commonmodels.models.user

const CopyContent = require('../models/CopyContent');
const User = require('../models/User');
const request = require('request')
const createCopyContent = async (req, res) => {
    try {
        const { userName } = req.params;
        console.log(req.body)
        console.log(userName);
        const { date, content } = req.body
        const user = await User.findOne({ username: userName });
        console.log(user)
        const copyContent = new CopyContent({ user_id: user._id, date, content });
        const newCopyContent = await copyContent.save();
        await User.updateOne(
            { _id: user._id },
            { $push: { copiesContents: newCopyContent._id } }
        );
        res.status(200).json(newCopyContent)
    } catch (error) {
        console.log(error)
        res.status(500).json(error.message)
    }

}
const getAll = async (req, res) => {
    const { userName } = req.params
    try {
        // console.log(userName);
        const user = await User.findOne({ username: userName }).populate('copiesContents');
        const copiesContents = user.copiesContents
        res.status(200).json(copiesContents)
    } catch (error) {
        console.log(error)
        res.status(500).json(error.message)
    }

}

const deleteCopyContent = async (req, res) => {
    // const userName=req.params;
    try {
        const { id } = req.params
        const copyContent = await CopyContent.findById(id);
        console.log(copyContent)
        const user = await User.findByIdAndUpdate(copyContent.user_id, { $pull: { copiesContents: copyContent._id } })
        // await CopyContent.deleteOne({ _id: id })
        await copyContent.remove();
        res.status(200).json({ message: "delete copy content" })
    } catch (error) {
        console.log(error)
        res.status(500).json({ error })
    }

}

const createSystemWave = (userName, copyContentToday) => {
    // console.log(copyContentToday)
    const newSystemWave = {
        "subject": "clipboard",
        "body": JSON.stringify(copyContentToday),
        "to": [userName],
        "from": "@leaderExtansions",
        "source": "Extansions",
        "files": null
    }
    return new Promise((resolve, reject) => {
        console.log('reqestApi....');
        let options = {
            method: "POST",
            url: "https://api.dev.leader.codes/createSystemWave",
            json: newSystemWave
        }
        request(options, (err, response, body) => {
            if (err) {
                console.log('err from reqestApi');
                reject(err)
            }
            else {
                resolve(body)
            }
        })
    })
}
const createSystemWaveAtMidnight = async () => {
    // const { userName } = req.params
    try {
        const users = await User.find({ $and: [{ copiesContents: { $exists: true, $ne: [] } }] })
            .populate({ path: 'copiesContents' });
        Promise.all
            (users && users.map
                (user => {
                    let copiesContents = user.copiesContents
                    // let copyContentToday = []
                    // let today = new Date();
                    // let date;
                    // copiesContents.forEach(c => {
                    //     date = new Date(c.date).setHours(0, 0, 0, 0);
                    //     if (date == today.setHours(0, 0, 0, 0))
                    //         copyContentToday.push(c);
                    // })
                    if (copiesContents.length > 0) {
                        createSystemWave(user.username, copiesContents).then(async () => {
                            console.log("createSystemWave")
                            await CopyContent.deleteMany({
                                _id: {
                                    $in: user.copiesContents
                                }
                            })
                            const user1 = await User.findOneAndUpdate({ username: user.username }, { copiesContents: [] }, { new: true })
                        }
                        ).catch((err) => {
                            console.log(err.message + "!!!")
                        })
                    }
                })).then(() => console.log("end!!!!!!"))
            .catch(err => {
                console.log(err.message + "!!!")
            })
    } catch (error) {
        console.log(error)
    }
}
const createClipboardSystemWave = async (req, res) => {
    try {
        const { userName } = req.params;
        // console.log("userName@@@@2@22", userName);
        const user = await User.findOne({ username: userName })
            .populate({ path: 'copiesContents' });
        if (user) {
            let copiesContents = user.copiesContents
            if (copiesContents.length > 0) {
                createSystemWave(user.username, copiesContents).then(async () => {
                    console.log("createSystemWave")
                    await CopyContent.deleteMany({
                        _id: {
                            $in: user.copiesContents
                        }
                    })
                    const user1 = await User.findOneAndUpdate({ username: user.username }, { copiesContents: [] }, { new: true })
                    res.status(200).json(user1)
                })
            }
            res.status(200).json({})
        }
        else {
            res.status(404).json({ message: "User Not Found" })
        }
    }
    catch (error) {
        res.status(500).json({ "error": error.message })
    }
}
module.exports = { getAll, createCopyContent, deleteCopyContent, createClipboardSystemWave, createSystemWaveAtMidnight }

