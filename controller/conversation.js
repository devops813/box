// const commonmodels = require('@leadercodes/modelsnpm');
// const Contact = commonmodels.models.contact
// const User= commonmodels.models.user
// const Wave = commonmodels.models.wave
// const Conversation= commonmodels.models.conversation
// const Ticket = commonmodels.models.ticket
// const Source= commonmodels.models.source
// const Tag = commonmodels.models.tag


const Contact = require('../models/Contact');
const Wave = require('../models/Wave');
const Conversation = require('../models/Conversation');
const User = require('../models/User')
const Ticket = require('../models/Ticket')
const Source = require('../models/Source')
const Tag = require('../models/Tag')



const request = require('request');
var async = require('async');
const utf8 = require('utf8');

module.exports = {
    //need to move to the Master
    //save conversation and wave to user
    //no email sent and no contact created
    //if sendEmailToUser=true, will send email.
    saveConversationGlobal: async (req, res) => {
        const { conversation, wave, sendEmailToUser = false } = req.body;
        const jwt = req.headers.authentication;
        const Cuser = await User.findOne({ username: req.params.username });
        const userID = Cuser._id;
        var newConversation = new Conversation(conversation)
        var newWave = new Wave(wave);
        newConversation.user = userID;
        newConversation.waves.push(newWave._id)
        newWave.conversation = newConversation._id
        Cuser.conversations.push(newConversation._id)

        if (sendEmailToUser == true) {
            var sendEmail = { to: Cuser.email, from: 'noReply@mail.leader.codes', subject: newConversation.subject, html: newWave.body }
            sendMessage(sendEmail, jwt)
        }

        await Cuser.save().then((result) => {
        }).catch((err) => {
            err
        })

        await newWave.save().then((result) => {
        }).catch((err) => {
            err
        })
        await newConversation.save().then((result) => {
            res.status(200).json({ result })
        }).catch((err) => {
            err
        })

    },

    //exist in master!!!!!!!!!!!!!
    //recive conversation from contact
    //not save the email to the contact alghout if he also user-- meantime!
    getConversationFromContact: async (req, res) => {
        const { conversation, wave, contact, messageToContact, sendSubmitToContact = true } = req.body;
        const Cuser = await User.findOne({ username: req.params.username });
        const userID = Cuser._id;
        const jwt = req.headers.authentication;
        let sendEmail = {};
        let newContact;
        var newConversation = new Conversation(conversation)
        var newWave = new Wave(wave);

        sendEmail.html = newWave.body;
        sendEmail.subject = newConversation.subject;
        sendEmail.to = Cuser.email;
        newConversation.waves.push(newWave._id);
        newConversation.user = userID
        newWave.conversation = newConversation._id;
        newWave.accepted = true;

        if (contact.email) {
            console.log("contact/email", contact.email);
            sendEmail.from = contact.email;

            newContact = await Contact.findOne({ user: userID, email: contact.email });
            if (!newContact) {
                newContact = new Contact(contact)
                newContact.user = userID;
                if (newContact.name == null)
                    newContact.name = contact.email.substring(0, contact.email.indexOf('@'))
                //add contat to user's contacts
                Cuser.contacts.push(newContact._id)

                newConversation.contacts.push(newContact._id);
                newContact.conversations.push(newConversation._id)
                await newContact.save().then((contact) => {
                }).catch((err) => {
                    err
                })
            }//if
        }//if
        else
            sendEmail.from = 'unknown@mail.leader.codes'
        // saveConversationToReciver(newContact, conversation, wave, Cuser)


        Cuser.conversations.push(newConversation)
        // console.log("sendEmail", sendEmail);
        // sendMessage(sendEmail, jwt)

        // if (conversation.source in ('forms', 'landingPage')) {
        //     if (sendSubmitToContact == true && contact.email) {
        //         var sendEmail2 = { to: contact.email, from: 'noReply@mail.leader.codes', subject: 'Leader.Codes', html: messageToContact }
        //         sendMessage(sendEmail2, jwt)
        //     }
        // }
        await Cuser.save();
        await newWave.save();
        let result = await newConversation.save();
        if (newContact) {
            //console.log("email save conversion", result);
            let contactId = newContact._id;
            res.status(200).json({ result, contactId })
        }
        //  Cuser.save().then((result2) => { 
        //      newWave.save().then((result) => {
        //          newConversation.save().then((result) => {
        //             if (newContact) {
        //                 console.log("email save conversion",result);
        //                 let contactId = newContact._id;
        //                 res.status(200).json({ result, contactId })
        //             }
        //             else
        //                 res.status(200).json({ result })

        //         }).catch((err) => {
        //             err
        //         })
        //     }).catch((err) => {
        //         err
        //     })
        // })
        //     .catch((err) => {
        //         err
        //     })
    },

    //need to move to the Master
    // just to leader box-
    //create cnversation to user and to all contact that are users
    //create contacts
    newConversation: async (req, res) => {
        //emails= wave.to
        const { conversation, wave, forward } = req.body;
        //find user by the params and initializing the variables
        let Cuser
        //if manager sent for him user
        if (wave.from) {
            Cuser = await User.findOne({ username: wave.from.split("@mails.codes")[0] })
        }
        else {
            Cuser = await User.findOne({ username: req.params.username })
            console.log("the username", Cuser.username);
            wave.from = Cuser.username;
        }
        const userID = Cuser._id;
        console.log(userID)
        const jwt = req.headers.authorization;
        let sendEmail = {};
        let newContactsToSend = []
        var myConversation = new Conversation(conversation)
        // when send mail to himself the email is readed
        if (wave.to.includes(`${req.params.username}@mails.codes`)) {
            myConversation.readed = false;
        }
        else {
            myConversation.readed = true;
        }
        // wave.fromId = userID
        var newWave = new Wave(wave);
        myConversation.waves.push(newWave._id);
        myConversation.user = userID
        // myConversation.rootConversationID = myConversation._id;
        // if (conversation.rootConversationID == null)
        //     conversation.rootConversationID = myConversation._id;
        newWave.conversation = myConversation._id;
        const groups = Cuser.groups
        //function Send to all recipients
        // newContactsToSend =
        await sendToAllRecipients(newWave, Cuser, userID, conversation, jwt, wave, myConversation, Cuser.username, forward, newContactsToSend, res);

    },


    //need to move to the Master
    //function to whats up - to get conversation by source and subject
    getConversationBySubject: async (req, res) => {
        const Cuser = await User.findOne({ username: req.params.username });
        const userID = Cuser._id;
        const { subject, source } = req.body;
        Conversation.findOne({ user: userID, subject: subject, source: source }).then((con) => {
            var conversationId = con._id;
            res.status(200).json({ conversationId })
        }).catch((err) => {
            res.status(500).json({ err, message: "cant find conversation" })
            //   res.status(500).json({ 0})
            // return 0;

        })
    },

    getAllConversations: async (req, res) => {
        const Cuser = await User.findOne({ username: req.params.username });
        const userID = Cuser._id;
        console.log("userID!!!!!!!!!!", userID);
        var Conversations = [];
        //  console.log("userid", userID);
        Conversation.find({ user: userID /*, archived: { $ne: true }*/ }).populate('waves').populate('contacts').then((ConversationsList) => {
            ConversationsList.forEach(async (element) => {
                await element.waves.forEach((wave) => {
                    if (wave.from && wave.from != Cuser.username && !wave.from.includes('@')) {
                        User.findOne({ username: wave.from }).then((res1) => {
                            Contact.findOne({ email: res1 ? res1.email : null, user: Cuser }).then((contact) => {
                                wave.nameFrom = contact.name;
                                wave.colorFrom = contact.color;
                                wave.save();
                            })
                        })
                    }
                    else if (wave.from && wave.from != Cuser.username + '@mail.leader.codes' && wave.from != Cuser.username)
                        Contact.findOne({ email: wave.from, user: Cuser._id }).then((contact) => {

                            if (contact) {
                                wave.nameFrom = contact.name;
                                wave.colorFrom = contact.color;
                            }

                        })


                })
                Conversations.push(element)

            })
            Ticket.find({ user: userID }).populate('wave').populate('contact').then(async (Tickets) => {
                await Tickets.forEach((element) => { Conversations.push(element) })
                res.status(200).json({ Conversations })

            }).catch((err) => {
                res.status(500).json({ message: 'failed to find conversations', err })
            })
        }).catch((err) => {
            res.status(500).json({ message: 'failed to find conversations', err })
        })
    },

    deleteSystemWaves: (req, res) => {
        console.log("ids", req.body.ids)
        const options = {
            url: `https://api.dev.leader.codes/${req.params.username}/deleteSystemWaves`,
            headers: { Authorization: req.headers.authorization },
            method: "DELETE",
            json: req.body,
        };
        request(options, (error, res1, body) => {
            if (error) {
                console.error(error);
            }
            if (res1 && !res1.statusCode == 200) {
                res.status(res1.statusCode).send(body)
            }
            res.status(res1.statusCode).send(body)
        });
    },

    createSystemWave: (req, res) => {
        const options = {
            url: `https://api.dev.leader.codes/createSystemWave`,
            method: "POST",
            json: req.body,
        };
        request(options, (error, res1, body) => {
            if (error) {
                console.error(error);
            }
            if (res1 && !res1.statusCode == 201) {
                res.status(res1.statusCode).send(body)
            }
            res.status(res1.statusCode).send(body)
        });
    },



    //not in use now!
    //the function remove conversation, its waves and pull it 
    //from user's list(in middleare in converrsation model)
    deleteConversations: async (req, res) => {
        const { conversationsID } = req.body;
        var err = false;
        var ind = 0;
        var length = conversationsID.length;
        await conversationsID.forEach(async function (element, index, conversationsID) {
            await Conversation.findOne({ _id: element })
                .then(async (result) => {
                    await result.remove().then((res2) => {
                        console.log("arrived to then!!");
                        ind = index;
                    }).catch(error => {
                        err = true;
                        return res.status(500).json({
                            error,
                            message: 'failed to delete some or all conversations'
                        })
                    })

                }).catch(error => {
                    err = true;
                    return res.status(500).json({
                        error,
                        message: 'failed to find some or all conversations'
                    })
                })
            console.log("if!!", err, ind);
            if (err == false && ind == length - 1) {
                return res.status(200).json({
                    message: 'conversations deleted'

                })
            }

        })
    },

    deleteConversation: (req, res) => {
        const { conversationID } = req.body;
        Conversation.deleteOne({ _id: conversationID }).then((result) => {
            return res.status(200).json({
                message: 'deleted'
            })
        }).catch(error => {
            return res.status(500).json({
                error,
                message: 'failed delete conversation'
            })
        })
    },

    archiveConversations: async (req, res) => {
        const { conversationsID } = req.body;
        var err = false;
        var ind = 0;
        var length = conversationsID.length;
        await conversationsID.forEach(async function (ID, index, conversationsID) {
            ind = index;
            await Conversation.findById(ID).then(async (result) => {
                if (result.archived == true)
                    return res.status(500).json({
                        message: 'conversation already archived'
                    });
                result.archived = true;
                await result.save().then((result2) => {
                }).catch(error => {
                    err = true;
                    console.log("error1", error)
                    return res.status(500).json({
                        error
                    });
                })
            }).catch(error => {
                err = true;
                return res.status(500).json({
                    error
                })

            })
            if (err == false && ind == length - 1) {
                return res.status(200).json({
                    message: 'conversations archived'

                })
            }
        });



    },
    //archives one conversation
    // archiveConversation: (req, res) => {
    //     const { conversationID } = req.body;
    //     console.log("in archiveConversation", conversationID)
    //     Conversation.findById(conversationID).then((result) => {
    //         console.log(result)
    //         result.archived = true;
    //         result.save();
    //         res.status(200).json({
    //             message: 'conversation archived',
    //             result
    //         })
    //     }).catch(error => {
    //         console.log("error", error)
    //         res.status(500).json({
    //             error
    //         })
    //     })
    // },
    editConversation: (req, res) => {
        console.log("in edit conversation")
        console.log("body", req.body);
        const { conversation } = req.body;
        console.log("conversation", conversation);
        Conversation.findById(conversation._id).then((result) => {
            console.log("find conversation")
            result.set(conversation);
            result.save();
            res.status(200).json({
                message: 'conversation updated',
                result
            })
        }).catch(error => {
            res.status(500).json({
                error
            })
        })
    },

    // editConversations: async (req, res) => {
    //     console.log("in edit conversations from react")
    //     const { conversations } = req.body;
    //     let editedConversations = []
    //     conversations.forEach(conversation => {
    //         Conversation.findById(conversation._id).then((result) => {
    //             console.log("find conversation")
    //             if (conversation.tag === "")
    //                 conversation.tag = null
    //             if (conversation.tag) {
    //                 console.log("tagggggg" + conversation.tag)
    //                 let waveId = result.waves[0]
    //                 Wave.findById(waveId).then(async (wave) => {
    //                     let contactFrom = wave.from
    //                     if (!contactFrom.match('@')) {
    //                         contactFrom = contactFrom + "@mails.codes"
    //                     }
    //                     console.log(result.user)
    //                     let contact = await Contact.findOne({ user: result.user, email: contactFrom })
    //                     if (!contact) {
    //                         console.log("contact not found")
    //                         let newContact = await new Contact()
    //                         newContact.email = contactFrom
    //                         newContact.name = await contact.contactFrom.substring(0, contactFrom.indexOf('@'))
    //                         await newContact.conversations.push(conversation._id)
    //                         newContact.save()
    //                         contact = newContact
    //                     }
    //                     // else {
    //                     console.log("contact found")
    //                     let tag = contact.tags.filter(tag => tag == conversation.tag)
    //                     if (tag.length == 0) {
    //                         contact.tags.push(conversation.tag)
    //                         contact.save()
    //                         //add to contactsTag the contact
    //                         await Tag.findById(conversation.tag).then((tagToAddContact) => {
    //                             tagToAddContact.contacts.push(contact._id)
    //                             tagToAddContact.save()
    //                         }).catch(error => {
    //                             console.log("tag not found")

    //                         })
    //                         console.log("tagAddSuccessfully")
    //                     }
    //                     else {
    //                         console.log("have a same tag")
    //                     }

    //                     // }
    //                 })
    //             }
    //             console.log("conversation", conversation);

    //             result.set(conversation);
    //             result.save();
    //             editedConversations.push(result)
    //             if (editedConversations.length === conversations.length) {
    //                 console.log("edit con")
    //                 res.status(200).json({
    //                     message: 'conversations updated',
    //                     editedConversations
    //                 })
    //             }
    //         }).catch(error => {
    //             res.status(500).json({
    //                 error
    //             })
    //         })
    //     })
    // },

    editConversations: async (req, res) => {
        const { conversations } = req.body;
        let editedConversations = []
        conversations.forEach(conversation => {
            Conversation.findById(conversation._id).then(async (result) => {
                let waveId = result.waves[0]
                await Wave.findById(waveId).then(async (wave) => {
                    let contactFrom = wave.from
                    if (!contactFrom.match('@')) {
                        contactFrom = contactFrom + "@mails.codes"
                    }
                    let contact = await Contact.findOne({ user: result.user, email: contactFrom })
                    if (!contact) {
                        console.log("contact not found")
                        let newContact = await new Contact()
                        newContact.email = contactFrom
                        newContact.name = await newContact.email.substring(0, contactFrom.indexOf('@'))
                        await newContact.conversations.push(conversation._id)
                        newContact.save()
                        contact = newContact
                    }
                    // else {
                    console.log("contact found")

                    // remove all tags from conversation
                    if (conversation.tag === "") {
                        console.log("remove all tags from conv")
                        // contact.tags.forEach(async(ct) => {
                        //     console.log(ct)
                        //     await Tag.findById(ct).then((tagToDeleteContact) => {
                        //         console.log("tagToDeleteContact", tagToDeleteContact)
                        //         console.log("contact", contact)
                        //         tagToDeleteContact.contacts = tagToDeleteContact.contacts.filter(c => c !== contact._id)
                        //         tagToDeleteContact.save()
                        //         console.log("tagToDeleteContactDelete", tagToDeleteContact)
                        //     })
                        //     .catch(error => {
                        //         console.log("tag not found")
                        //     })
                        // })
                        console.log("result.tags", result.tags)
                        result.tags.forEach(async (rt) => {
                            console.log(rt)
                            await Tag.findById(rt).then((tagToDeleteContact) => {
                                tagToDeleteContact.contacts = tagToDeleteContact.contacts.filter(c => c == contact._id)
                                tagToDeleteContact.save()
                            })
                                .catch(error => {
                                    console.log("tag not found")
                                })
                        })
                        contact.tags = []
                        contact.save()
                        result.tags = []
                        await result.save()
                        console.log("result after tags remove ", result)
                    }
                    // add tag
                    else {
                        if (!result.tags.find(t => t == conversation.tag)) {
                            console.log("add tag to array", result)
                            result.tags.push(conversation.tag)
                            result.save()
                            console.log(result)
                            let tag = contact.tags.filter(tag => tag == conversation.tag)
                            if (tag.length == 0) {
                                contact.tags.push(conversation.tag)
                                contact.save()
                                //add to contactsTag the contact
                                await Tag.findById(conversation.tag).then((tagToAddContact) => {
                                    tagToAddContact.contacts.push(contact._id)
                                    tagToAddContact.save()
                                }).catch(error => {
                                    console.log("tag not found")
                                })
                                console.log("tagAddSuccessfully")
                            }
                            else {
                                console.log("have a same tag")
                            }
                        }
                        // delete tag
                        else {
                            console.log("delete tag from array")
                            result.tags = result.tags.filter(t => t != conversation.tag)
                            result.save()
                            contact.tags = contact.tags.filter(t => t != conversation.tag)
                            contact.save()
                            await Tag.findById(conversation.tag).then((tagToDelContact) => {
                                tagToDelContact.contacts = tagToDelContact.contacts.filter(c => c != contact._id)
                                tagToDelContact.save()
                            }).catch(error => {
                                console.log("tag not found")

                            })
                        }
                    }
                })
                result.set(conversation);
                editedConversations.push(result)
                if (editedConversations.length === conversations.length) {
                    console.log("edit con")
                    res.status(200).json({
                        message: 'conversations updated',
                        editedConversations
                    })
                }
            }).catch(error => {
                res.status(500).json({
                    error
                })
            })
        })
    },
    // @shulamit cohen
    markAsReadOrUnreadConversations: async (req, res) => {
        const conversationsFromClient = req.body.conversations;
        let editedConversations = [];
        try {
            async.forEach((conversationsFromClient), async (c) => {
                let conversationInServer = await Conversation.findById(c._id);
                conversationInServer.readed = c.readed;
                await conversationInServer.save();
                editedConversations.push(conversationInServer);
            }, async () => {
                res.status(200).json({ conversations: editedConversations });
            })
        } catch (error) {
            res.status(500).json({ error })
        }
    },
    //sendMessage
    getAllSources: (req, res) => {
        Source.find().then((sources) => {
            res.status(200).json({ sources })
        }).catch((err) => {
            err;
        })
    },

    //add func with array of src to the Master that add all the src
    newSource: (req, res) => {
        console.log("----------------------new source!!!!!!!!!!");
        const { source1 } = req.body;
        console.log(source1);
        Source.insertMany(source1)
            .then((result) => {
                // Tag.insertMany(tag)
                //  newSource = new Source(source)
                res.status(200).json({ result })
            }).catch((err) => {
                // res.status(500).json({ err })
                // err;
                console.log(err);
            })
    },


    sendConversationByEmail: async (req, res) => {
        console.log("----------------------in send email!!!!!!!!!!");
        let { subject, messageBody, emailTo, username, references, inReplyTo, attachments } = req.body;
        let tempEmailTo = []
        await emailTo.map(async (to) => {
            //need to send to group members!!
            if (!(to.includes('@'))) {
                checkIfEmailIsGroup(to, username, tempEmailTo, req, res)

                // await console.log("nnnnn", );
            }
            else if (!to.includes('@mails.codes'))
                await tempEmailTo.push(to)
        })
        if (tempEmailTo.length === 1) {
            tempEmailTo = await tempEmailTo[0]
        }
        let result = await sendEmail(subject, messageBody, tempEmailTo, username, references, inReplyTo, attachments)
        if (result === "error")
            res.status(500).json("error while sending the message")
        else
            res.status(200).json(result)

    },
    // @shulamit cohen
    markAsTrashOrUntrashConversations: async (req, res) => {
        console.log('💚💙💜💛🧡💚💙💜💛🧡💚💙💜💛🧡💚💙💜💛🧡');
        const conversationsFromClient = req.body.conversations;
        let editedConversations = [];
        try {
            async.forEach((conversationsFromClient), async (c) => {
                let conversationInServer = await Conversation.findById(c._id);
                conversationInServer.trash.bool = !c.trash.bool;
                conversationInServer.trash.date = new Date();
                await conversationInServer.save();
                editedConversations.push(conversationInServer);
            }, async () => {
                res.status(200).json({ conversations: editedConversations });
            })
        } catch (error) {
            res.status(500).json({ error })
        }
    },
}


checkIfEmailIsGroup = async (to, username, tempEmailTo, req, res) => {

    await User.findOne({ username: username }).then(async (Cuser) => {
        // if ((Cuser.groups.length !== 0 && Cuser.groups[0] !== null)) {
        let myGroup = await Cuser.groups.find(g => g.groupName === to)
        //  }
        if (await myGroup) {
            async.forEach((myGroup.members), async (member) => {
                await Contact.findOne({ _id: member }).then(async (contact) => {
                    if (await (contact && !contact.email.includes('@mails.codes'))) {
                        await tempEmailTo.push(contact.email)
                        console.log("add member", contact.email);
                        console.log("temp email in func", tempEmailTo);

                    }
                })
            }, async () => {
                let { subject, messageBody, emailTo, username, references, inReplyTo, attachments } = req.body;

                if (tempEmailTo.length === 1) {
                    tempEmailTo = await tempEmailTo[0]
                }
                let result = await sendEmail(subject, messageBody, tempEmailTo, username, references, inReplyTo, attachments)
                if (result === "error")
                    res.status(500).json("error while sending the message")
                else
                    res.status(200).json(result)
            })
        }
    })

}

sendEmail = async (subject, body, emailTo, username, references, inReplyTo, attachments) => {
    console.log("in send mail!!", emailTo);
    if (attachments)
        attachments.map((file) => {
            file.filename = utf8.encode(file.filename)
            file.path = utf8.encode(file.path)
        })
    const email = {
        from: `${username}@mails.codes`,
        to: emailTo,
        subject: subject,
        html: body,
        references: references,
        inReplyTo: inReplyTo,
        attachments: attachments,
        isMarketing: false
    }
    const options = {
        url: 'https://mails.codes/mail/sendEmail',
        method: 'POST',
        headers: { Authorization: "secretKEY@2021" },
        json: email,
    };
    return new Promise((resolve, reject) => {
        request(options, (error, res, body) => {
            if (error) {
                console.error("error:" + error);
                reject("error");
            }
            console.log(`statusCode: ${res.statusCode}`);

            // console.log(body);
            resolve(res)
        });
    });

}

// move to Master
async function saveConversationToReciver(contact1, conversation, wave, contact) {
    console.log("arrive to saveConversationToReciver", contact1);
    let userNameVM = ''
    var user;
    if (contact1.email.includes('@mails.codes')) {
        userNameVM = await contact1.email.split('@mails.codes')[0]
        console.log("contact1.email.includes('@mails.codes')", userNameVM)
    }
    if (userNameVM !== '') {
        user = await User.findOne({ username: userNameVM });
    }
    // when user send mail to himself
    if (userNameVM === contact.username) {
        console.log("send to me");
        return;
    }
    // console.log("userrrr", user)
    //remove the option to send to contact in box with his assign mail
    // else
    //     user = await User.findOne({ email: contact1.email });
    if (!user)
        return null;
    console.log("contact exist as userrr");
    var newConversation = new Conversation(conversation)
    //Insert to the wave.body an empty img with a send to function to check if the email is opened
    wave.body = await "<div>" + wave.body + "<p>aaaaaaaa</p>" + "<img src='https://box.dev.leader.codes/api/tracker/" + wave.to + " '></div>";
    var newWave = new Wave(wave);
    console.log("try display wave " + wave);
    newConversation.waves.push(newWave._id);
    newConversation.user = user._id;
    newWave.conversation = newConversation._id;
    newWave.accepted = true;
    // wave.from- contact=user that sent the mail
    newWave.from = contact.username;
    console.log("before arrive to tag")
    let userReciver = await User.findOne({ username: contact1.email.split('@mails.codes')[0] })
    let contactReciver = await Contact.findOne({ user: userReciver._id, email: contact.username + "@mails.codes" })
    if (contactReciver && contactReciver.tags.length != 0) {
        console.log("arrive to tag!!!!!!!!!!!")
        console.log(contactReciver.tags)
        newConversation.tags = contactReciver.tags
    }

    // var newContact = await Contact.findOne({ user: user._id, email: contact.email })
    // if (!newContact) {
    //     const body =
    //     {
    //         "contact": {
    //             "email": contact.email,
    //         },
    //         "withConversation": false,
    //         "source": {
    //             "type": conversation.source
    //         }
    //     }
    //     console.log("lea user is now new ");
    //     // let f = await createNewContact(body, jwt, userName)

    //     // console.log("lea user is now new contact to contact!!", f);
    //     // var contact=User.findOne({_id:userID})
    //     // newContact = await new Contact()
    //     // newContact.user = await user._id;
    //     // newContact.email = await contact.email;
    //     // newContact.name = await contact.email.substring(0, contact.email.indexOf('@'))
    //     // //add contat to user's contacts
    //     // await user.contacts.push(newContact._id)
    // }

    // await newConversation.contacts.push(newContact._id);
    // await newContact.conversations.push(newConversation._id)
    // await user.conversations.push(newConversation)

    // await newContact.save().then((result) => {
    //     console.log("create contact");
    // }).catch((err) => {
    //     err
    // })
    await user.save().then((result) => {
        console.log("save user");
    }).catch((err) => {
        err
    })
    await newConversation.save().then((result) => {
        console.log("save conv");
    }).catch((err) => {
        err
    })
    await newWave.save().then((result) => {
        console.log("save wave");
    }).catch((err) => {
        err
    })
    return true
}


function createNewContact(contact, jwt, userName) {
    console.log("createNewContact", " username ", userName, " jwt ", jwt);
    const options = {
        url: `https://api.dev.leader.codes/${userName}/createContact`,
        headers: { Authorization: jwt },
        method: "POST",
        json: contact,
    };
    return new Promise(async (resolve, reject) => {
        request(options, (error, res, body) => {
            if (error) {
                console.error(error);
                reject(error);
            }
            if (res && !res.statusCode == 200) reject(body);
            console.log(`statusCode: ${res.statusCode}`);
            console.log(body);
            resolve(body);
        });
    });
}


//need to delete
function sendMessage(email, jwt) {
    console.log("email", email);
    console.log("jwt", jwt);

    console.log("arrive to sendEmail");

    request.post('https://api.leader.codes/mail/sendEmail', {
        json: email,
        headers: { Authorization: jwt }
    }, (error, res, body) => {
        console.log("arrive to sendEmail", body);

        if (error) {
            console.error(error)
            return
        }
        else
            return res

    })

}

//send To All Recipients
async function sendToAllRecipients
    (newWave, Cuser, userID, conversation, jwt, wave, myConversation, userName, forward, newContactsToSend, res) {
    console.log("newWave.to", newWave.to)
    Promise.all(
        newWave.to.map(async (email, index) => {
            console.log("email ", email);
            //if the 'email' is groupName = saving to each contact in the group
            let myGroup = null
            if (Cuser.groups.length !== 0 && Cuser.groups[0] !== null) {
                myGroup = await Cuser.groups.find(g => g.groupName === email)
            }
            // console.log("🚀 ~ file: conversation.js ~ line 975 ~ newWave.to.map ~ myGroup", myGroup)
            if (myGroup) {
                // await newWave.toId.push(myGroup._id)
                await myGroup.members.forEach(async (member) => {
                    let contact = await Contact.findOne({ _id: member })
                    await saveConversationToReciver(contact, conversation, wave, Cuser)
                })
            }
            else {
                //not group 
                //check if the userID and email exists in contact
                if (Cuser.email !== email) {
                    let newContact = await Contact.findOne({ user: userID, email: email });
                    //if not exists in contact -create contact
                    if (!newContact) {
                        console.log("the contact is not exist!!!!!!!!");
                        const body =
                        {
                            "contact": {
                                "email": email,
                            },
                            "withConversation": false,
                            "source": {
                                "type": conversation.source
                            }
                        }
                        let tempContact = await createNewContact(body, jwt, userName)
                        // console.log("tempContact", tempContact)
                        // await newWave.toId.push(tempContact.newContact._id)
                        await newContactsToSend.push(tempContact.newContact)
                        newContact = await tempContact.newContact
                        console.log("nnewContactsToSend in func", newContactsToSend);
                        console.log(newWave, "after save contact");
                        // if (!forward) {
                        console.log("want to save Conversation To Reciver", newContact)
                        if (newContact && newContact.email)
                            saveConversationToReciver(newContact, conversation, wave, Cuser)
                        // }
                    }
                    else if (newContact) {
                        console.log("from elseeee");
                        await saveConversationToReciver(newContact, conversation, wave, Cuser)
                    }
                }
            }
        }
        )).then(async () => {
            console.log("contacts created successfully")
            if (!forward) {
                await Cuser.conversations.push(myConversation)
                console.log("not forward!!!!!!");

                //for meanwile- till the mails will work good
                // sendMessage(sendEmail, jwt)
                console.log("newContactsToSend111111111111111111", newContactsToSend)
                console.log("user to send", Cuser);
                await Cuser.save().then((result2) => {
                    console.log("save user");
                }).catch((err) => {
                    console.log("err in save user", err);
                    res.status(500).json({
                        err,
                        message: 'Cuser.save()'
                    })
                })

                // return newContactsToSend
                console.log("after createContacts", newContactsToSend);
                await newWave.save().then((result) => {
                }).catch((err) => {
                    res.status(500).json({
                        err,
                        message: 'newWave.save()'
                    })
                })

                await myConversation.save().then(async (result) => {
                    var newConversation2 = await Conversation.findById({ _id: myConversation._id }).populate('contacts').populate('waves');
                    console.log("newContactsToSend", newContactsToSend)
                    res.status(200).json({
                        newConversation: newConversation2, //shir=> I added this because the server was collapsing. check it out
                        newContactsToSend: newContactsToSend//michal giladi=> I added this to get the new contacts :)
                    })
                }).catch((err) => {
                    res.status(500).json({
                        err,
                        message: 'newConversation.save()'
                    })
                })
            }
        })

}


