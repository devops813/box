const mongoose = require('mongoose')
const Wave = require('./Wave');
const User = require('./User')

const conversationSchema = new mongoose.Schema({
    //have to be deleted- contact
    contacts: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Contact'
    }],
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    source: String,
    //marked: Boolean,
    subject: String,
    readed: { type: Boolean, default: false },
    sourceIP: String,
    waves: [
        { type: mongoose.Schema.Types.ObjectId, ref: 'Wave' }
    ],
    tag: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Tag',
        null: true
    },
    tags: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Tag',
        null: true
    }],

    //for anonymous conversations
    ipConnect: String,
    starred: Boolean,
    archived: Boolean,
    flagged: { bool: { type: Boolean, default: false }, date: { type: Date, default: Date.now() } },
    spam: { type: Boolean, default: false },
    fwd: { type: Boolean, default: false },
    pin: { type: Date, default: null },
    trash: { bool: { type: Boolean, default: false }, date: { type: Date, default: Date.now() } },
    messageId: String
}).pre("remove", function (next) {

    User.findByIdAndUpdate({ _id: this.user }, { $pull: { conversations: this._id } }).then((res) => {
        console.log("res2", res);

    }).catch((err) => { console.log(err); })

    this.waves.forEach(element => {
        Wave.findByIdAndDelete({ _id: element }).then((res) => {
            // console.log("res", res);
        }).catch((err) => { console.log(err); })
    });
    next();
})


module.exports = mongoose.model('Conversation', conversationSchema)

conversationSchema.pre('findByIdAndDelete', function (next) {
    console.log("##################");
    // waves.forEach(element => {
    // Wave.findByIdAndDelete(element).exec();
    console.log("succeded to delete wave!");
    // next();

    //  });

})


