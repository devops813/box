const mongoose = require('mongoose')

const waveSchema = new mongoose.Schema({
    timestamp: { type: Date, default: Date.now },
    body: String,
    accepted: Boolean,
    files: [{ name: String, url: String }],
    conversation: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Conversation'
    },
    from: String,
    // fromId: { type: mongoose.Schema.Types.ObjectId },
    to: [String],
    // toId: [{ type: mongoose.Schema.Types.ObjectId }],
    cc: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Contact'
    }],
    bcc: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Contact'
    }],
    nameFrom: String,
    colorFrom: String
})

module.exports = mongoose.model('Wave', waveSchema)