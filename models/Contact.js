const mongoose = require('mongoose')
//const User = require('./User')
const Contact = require('./Contact')
//const ContactGoogle = require('./ContactGoogle')
const contactSchema = new mongoose.Schema({
    name: { type: String },
    email: {
        type: String, require: true,
        match: /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    },
    phone: {
        type: String,
        //  required: true,
        match: /\d{10}/,
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    thumbnail: String,
    numOfUnReadedWaves: Number,
    status: String,
    sourceUrl: String,
    displayToUser: { type: Boolean, default: true },
    conversations: [
        { type: mongoose.Schema.Types.ObjectId, ref: 'Conversation' }
    ],
    source: [s = new mongoose.Schema({
        id: { type: mongoose.Schema.Types.ObjectId },
        type: String
    })],
    //  type: String,
    // sourceID: [{ type: mongoose.Schema.Types.ObjectId }],
    leadOwner: String,
    leadSource: String,
    customerType: String,
    companySize: String,
    companyName: String,
    gender: String,
    createDateAndTime: { type: Date, default: Date.now },
    bestTimeToCall: Date,
    birthday: Date,
    telephon: String,
    mobileNumber: String,
    companyAddress: String,
    state: String,
    zipcode: String,
    website: String,
    whatsapp: String,
    linkedIn: String,
    facebook: String,
    instagram: String,
    youTube: String,
    color: String,
    googleContact: { type: mongoose.Schema.Types.ObjectId, ref: 'ContactGoogle' },
    tags: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Tag' }]


})

// .pre("save", async function (next) {
//     // const colors = ["#93389D", "#620E88", "#381AA2", "#393696", "#2062A0", "#2792A0", "#2C8D78", "#468B13", "#808E14", "#808E14", "#808E14", "#B1651A", "#AA3316", "#A01B4B", "#B2434C",
//     //     "#FD80E5", "#B620E0", "#6236FC", "#8580FD", "#3598F4", "#40D9ED", "#44D7B6", "#42C153", "#BFD41F", "#F0D923", "#F8B520", "#F88C20", "#F84A20", "#F13B7F", "#FD808B"]

//     let letters = '0123456789ABCDEF';
//     let color = '#';
//     for (var i = 0; i < 6; i++) {
//         color += letters[Math.floor(Math.random() * 16)];
//         color = "#2259E9"
//     }
//     let x = await contactSchema.methods.checkColors2(this, color)
//     let answerTemp = x;
//     let j = 0
//     while (answerTemp === false) {
//         j++

//         color = '#';

//         for (var i = 0; i < 6; i++) {
//             color += letters[Math.floor(Math.random() * 16)];
//         }
//         if (j === 1) {
//             color = "#2259E9"
//         }
//         //console.log("colorTemp", color)


//         x = await contactSchema.methods.checkColors2(this, color);
//         answerTemp = x;
//     }


//     // x = await contactSchema.methods.checkColors2(this, color)
//     // console.log("checkColors(this, color)1111111111111", x);

//     // let j = 0
//     // let answer = x
//     // while (!answer && j < 3) {
//     //     answer = await contactSchema.methods.checkColors2(this, color);
//     //     console.log('answer', answer);

//     //     j++
//     //     color = '#';
//     //     console.log("arrived to while!!!!!!!!!!!!!!!");
//     //     for (var i = 0; i < 6; i++) {
//     //         color += letters[Math.floor(Math.random() * 16)];
//     //     }
//     //     console.log("color", color)
//     //     // await checkColors(this, color)
//     // }
//     // console.log("checkColors(this, color)", await contactSchema.methods.checkColors2(this, color));
//     // //if (checkColors2(this, color) != false) {
//     this.color = color;
//     // console.log("arrived to pre");

//     next();
//     //    }

// })

let index;

async function getRandomColor(contact) {
    console.log("arrived to getRandomColor");
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
        color = "#2259E9"
    }
    // console.log("return", checkColors(contact, color));

    while (checkColors(contact, color) == false) {
        color = '#';
        console.log("arrived to while!!!!!!!!!!!!!!!");
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        // checkColors(contact, color)
        if (checkColors(contact, color) != false) {
            console.log("return color");

        }
        return true;
    }


    //if(index)
    // console.log("return color");
    // return color;
}
async function checkColors(contact, color) {
    console.log("contact", contact);
    //  index = true;
    var length;
    var ind = 0;
    //var index=true;
    //  console.log("arrived to checkColors", color);

    await User.findOne({ _id: contact.user }, { contacts: 1 }).populate("contacts").then(async (contacts) => {
        // console.log("contacts", contacts.contacts);
        console.log("contacts.contacts.length", contacts.contacts.length);
        length = contacts.contacts.length;
        for await (const element of contacts.contacts) {
            console.log("element,element", element);
            if (element.color == color) {
                ind++;
                index = false;
                console.log("falseeeeeeeeeeeeeeee");
                // index = false;
                return false;
            }
            else {
                console.log("elseee");
                ind++;

            }
            if (index == false)
                return false;
        }
    });
    console.log("ind", ind);
    console.log("length", length);

    if (ind == length - 1 && index !== false) {
        index = true
        return true

    }
    else //if(ind==length &&index===false)
        return false;


}
// BookSchema.methods.findSimilar = function(callback) {
//     Book.find({genre: this.genre}).exec(function doThings(err, doc){ 
//         /* ... */ 
//     }); 
contactSchema.methods.checkColors2 = async function (contact, color) {
    //   async function checkColors2 (contact, color){

    var length;
    var ind = 0;
    // User.findOne({ _id: contact.user }, { contacts: 1 }).populate("contacts").then(async(contacts) => {
    // // console.log("contacts", contacts.contacts);
    // console.log("contacts.contacts.length",contacts.contacts.length);
    //  length=contacts.contacts.length;
    console.log(contact.model);
    const x = await contact.model('Contact').find({ user: contact.user, color: color });
    // console.log('hhhhh', x.length)
    if (x.length >= 1) {
        // console.log("falae false", x[0])
        return false
    }

    else {
        // console.log("truye true", x[0])
        return true
    }
    //    .then((res) => {
    //         return "hujgf"
    // return "dsfsd"
    // console.log("ressssssssssss", res);
    // if (res) {
    //     index = false
    //     return "false"

    // }
    // else {
    //     index = true
    //     return "true"

    // }
    // })
    // for await (const element of contacts.contacts) {
    //     console.log("element,element",element);
    //     if (element.color == color) {
    //         ind++;
    //         index=false;
    //          console.log("falseeeeeeeeeeeeeeee");
    //         // index = false;
    //         return false;
    //     }
    //     else{
    //         console.log("elseee");
    //     ind++;

    //     }
    //     if(index==false)
    //     return false;
    // }
    //  })
    // .catch((err) => {
    //     console.log(err);
    // });

    // console.log("ind",ind);
    // console.log("length",length);

    // if(ind==length-1 &&index!==false){
    //     index=true
    // return true

    // }
    // else //if(ind==length &&index===false)
    // return false;


}


module.exports = mongoose.model('Contact', contactSchema)

