const express = require('express');
const jwt = require('jsonwebtoken');
const mongoose = require('mongoose')
const dotenv = require('dotenv');
const bodyParser = require('body-parser')
const path = require('path')
const app = express();
const request = require('request');
const fileUpload = require("express-fileupload");
const cookieParser = require('cookie-parser')
const schedule = require('node-schedule');
const scheduleFunc = require('./schedule');
const useragent = require('express-useragent');
const { createSystemWaveAtMidnight } = require('./controller/copyContent')
const checkPermissionController = require('./controller/checkPermission')
const boxRouterApi = require('./routes/api')
const boxRouterViews = require('./routes/views')

dotenv.config();

//mongoose connect
const common = require('@leadercodes/modelsnpm');
common.init(mongoose);

//LOCALY CONNECT to mongoose
mongoose.connect(process.env.DB_CONNECT2, {}).then((res) => {
})


//middlewares
app.use(fileUpload({ createParentPath: true }));
app.use(cookieParser())
app.use(bodyParser.json({ limit: '200mb' }));

app.use(bodyParser.urlencoded({
  extended: true,
  defer: true
}));

app.get("/", (req, res, next) => {
  console.log("arrive to get");
  res.redirect("https://dev.accounts.codes/box/login");
})
app.use('/', boxRouterViews)
//need to add to the api - checkPermissionController.checkPermission
app.use('/api' ,boxRouterApi)
app.use('/:userName/isPermission', checkPermissionController.checkPermission, checkPermissionController.isPremission );
app.use(useragent.express());



app.listen(process.env.LISTEN_PORT, () => {
  console.log("server is up");
});

// run everyday at midnight
schedule.scheduleJob('10 58 20 * * *', (date) => {
  console.log("its work!!!!!!!!!!!");
  console.log("date@@@@@@@", new Date());
  createSystemWaveAtMidnight()
}) 