const mongoose = require('mongoose')
const Wave = require('./Wave');
const User = require('./User')

const draftSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    subject: String,
    to: [String],
    body: String,
    files: [String],
    timestamp: { type: Date, default: Date.now },
    starred: Boolean,
    archived: Boolean,
    //for 'reply draft'
    rootConversationID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Conversation'
    },
})

module.exports = mongoose.model('Draft', draftSchema)



