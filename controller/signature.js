// const commonmodels = require('@leadercodes/modelsnpm');
// const User = commonmodels.models.user
// const Signature = commonmodels.models.signature



const Signature = require('../models/Signature');
const User = require('../models/User');



const request = require('request');
const changeDisplayForEmptyData = (signature) => {
    console.log("in changeDisplayForEmptyData")
    if (!signature.twitter_icon.url) {
        signature.twitter_icon.display = false
    }
    if (!signature.youtube_icon.url) {
        signature.youtube_icon.display = false
    }
    if (!signature.facebook_icon.url) {
        signature.facebook_icon.display = false
    }
    if (!signature.linkedin_icon.url) {
        signature.linkedin_icon.display = false
    }
    if (!signature.whatsapp_icon.url) {
        signature.whatsapp_icon.display = false
    }
    if (!signature.instagram_icon.url) {
        signature.instagram_icon.display = false
    }
    if (!signature.upload_photo.url) {
        signature.upload_photo.display = false
    }
    if (!signature.banner.imgUrl) {
        signature.banner.display = false
    }
}
module.exports = {
    newSignature: async (req, res) => {
        const Cuser = await User.findOne({ username: req.params.username });
        const userId = Cuser._id;
        console.log("userId", userId);
        let { signature } = req.body;
        changeDisplayForEmptyData(signature)
        console.log("signature", signature);
        const newSignature = new Signature(signature);
        console.log("newSignature", newSignature);
        newSignature.user = userId;
        newSignature.save().then((result) => {
            Cuser.signatures.push(newSignature._id);
            Cuser.save().then((result2) => {
                res.status(200).json({
                    newSignature,
                    message: 'Signature created'
                })
            }).catch((err) => {
                res.status(500).json({
                    err,
                    message: 'failed to save signature to user'
                })
            })
        }).catch((err) => {
            res.status(500).json({
                err,
                message: 'failed to create the signature'
            })
        })
    },
    editSignature: (req, res) => {
        let { signature } = req.body
        console.log("signature,,", signature)
        changeDisplayForEmptyData(signature)
        console.log(signature)
        Signature.findById({ _id: signature._id }).then((existSignature) => {
            existSignature.set(signature);
            existSignature.save();
            res.status(200).json({
                message: 'signature updated',
                existSignature
            })
        }).catch(error => {
            res.status(500).json({
                error,
                message: 'failed to find the signature'
            })
        })
    },

    // editSignatureByName: (req, res) => {
    //     const { signature } = req.body
    //     console.log("my sig " + signature.full_name);
    //     Signature.findOneAndUpdate(
    //         { signatureName: signature.signatureName },
    //         {
    //             '$set': {
    //                 full_name: signature.full_name,
    //                 role: signature.role, phone:
    //                     signature.phone, address:
    //                     signature.address, website:
    //                     signature.website,
    //                 main_color:
    //                     signature.main_color,
    //                 text_color: signature.text_color,
    //                 text_size: signature.text_size,
    //                 linkedin_icon: signature.linkedin_icon,
    //                 twitter_icon: signature.twitter_icon,
    //                 facebook_icon: signature.facebook_icon,
    //                 youtube_icon: signature.youtube_icon,
    //                 whatsapp_icon: signature.whatsapp_icon,
    //                 upload_photo: signature.upload_photo
    //             }
    //         },
    //         { 'new': true /*, 'upsert': true */ },
    //         function (err, updatedSignature) {
    //             if (err) {
    //                 console.log("err " + error);
    //                 res.status(500).json({
    //                     error,
    //                     message: 'failed to find the signature'
    //                 })
    //             }
    //             else {
    //                 res.status(200).json({
    //                     message: 'signature updated',
    //                     updatedSignature
    //                 })
    //             }

    //             //     var existSignature = data
    //             //     existSignature.full_name = signature.full_name
    //             //     existSignature.role = signature.role
    //             //     existSignature.phone = signature.phone
    //             //     existSignature.address = signature.address
    //             //     existSignature.website = signature.website
    //             //     existSignature.main_color = signature.main_color
    //             //     existSignature.text_color = signature.text_color
    //             //     existSignature.text_size = signature.text_size
    //             //     existSignature.linkedin_icon = signature.linkedin_icon
    //             //     existSignature.twitter_icon = signature.twitter_icon
    //             //     existSignature.facebook_icon = signature.facebook_icon
    //             //     existSignature.youtube_icon = signature.youtube_icon
    //             //     existSignature.whatsapp_icon = signature.whatsapp_icon
    //             //     existSignature.upload_photo = signature.upload_photo

    //         })
    // },

    getAllSignatures: async (req, res) => {
        User.findOne({ username: req.params.username }).then((Cuser) => {
            console.log("user", Cuser.username);
            Signature.find({ user: Cuser._id }).then((signatures) => {
                res.status(200).json({
                    signatures
                })
            }).catch((err) => {
                res.status(500).json({
                    err,
                    message: 'failed to find signature'
                })
            })
        }).catch((err) => {
            res.status(500).json({
                err,
                message: 'failed to find user'
            })
        })
    },
    //not in use
    // getSignatureByName: async (req, res) => {
    //     const signaturename = req.body.signaturename;
    //     User.findOne({ username: req.params.username }).then((Cuser) => {
    //         const userId = Cuser._id
    //         Signature.findOne({ signatureName: signaturename, user: userId }).then(signature => {
    //             if (signature)
    //                 res.json({
    //                     signature

    //                 })
    //             else
    //                 res.json({
    //                     message: 'signature is not exist'
    //                 })
    //         }).catch(err => {
    //             res.status(500).json({
    //                 err,
    //                 message: 'failed to find signature'
    //             })
    //         })
    //     }).catch(err => {
    //         res.status(500).json({
    //             err,
    //             message: 'failed fo find user'
    //         })
    //     })
    // },

    // deleteSignatureByName: async (req, res) => {
    //     console.log("in delete       !!!!!!!!!!", req.params.username);
    //     const signaturename = req.body.signaturename;
    //     console.log("signaturename", signaturename)
    //     User.findOne({ username: req.params.username }).then(async (Cuser) => {
    //         const userId = Cuser._id
    //         const signature = await Signature.findOne({ signatureName: signaturename, user: userId });
    //         console.log("find sig ", signaturename);
    //         console.log(signature._id);
    //         await User.updateOne({ username: req.params.username }, { $pull: { signatures: signature._id } });
    //         console.log(Cuser.signatures);
    //         Signature.findOneAndDelete({ signatureName: signaturename, user: userId }).then((result) => {
    //             console.log("find Signature")
    //             res.json({
    //                 message: 'success to delete signature'

    //             })
    //         }).catch(err => {
    //             res.status(500).json({
    //                 err,
    //                 message: 'failed to find signature'
    //             })
    //         })
    //     }).catch(err => {
    //         res.status(500).json({
    //             err,
    //             message: 'failed to find user'
    //         })
    //     })
    // },
    deleteSignature: async (req, res) => {
        console.log("in delete       !!!!!!!!!!", req.params.username);
        const signatureId = req.body.signatureId;
        console.log("signatureId", signatureId)
        User.findOne({ username: req.params.username }).then(async (Cuser) => {
            const userId = Cuser._id
            const signature = await Signature.findOne({ _id: signatureId, user: userId });
            console.log("find sig ", signature);
            await User.updateOne({ username: req.params.username }, { $pull: { signatures: signature._id } });
            console.log(Cuser.signatures);
            Signature.findOneAndDelete({ _id: signatureId, user: userId }).then((result) => {
                console.log("find Signature")
                res.json({
                    message: 'success to delete signature'

                })
            }).catch(err => {
                res.status(500).json({
                    err,
                    message: 'failed to find signature'
                })
            })
        }).catch(err => {
            res.status(500).json({
                err,
                message: 'failed to find user'
            })
        })
    },
    uploadPhoto: async (req, res) => {
        console.log("my fileeeeeee " + req.files.file)
        let url = await uploadedFile(req.files.file, req.params.uId, req.headers["authorization"]);
        console.log(url);
        res.send(url);
    }
}
const uploadedFile = (fileToUpload, uId, headers) => {
    console.log("headers", headers);
    return new Promise(async (resolve, reject) => {
        console.log(fileToUpload);
        console.log("uploadedFile");
        const uri = `https://files.leader.codes/api/${uId}/upload`;
        console.log(uri);
        const options = {
            method: "POST",
            url: uri,
            headers: {
                Authorization: headers,
                "Content-Type": "multipart/form-data",
            },
            formData: {
                file: {
                    value: fileToUpload.data,
                    options: {
                        filename: fileToUpload.name,
                    },
                },
            },
        };
        console.log("🚀 ~ file: signature.js ~ line 279 ~ returnnewPromise ~ options", options)

        request(options, async (err, res, body) => {
            if (err) {
                console.log(err);
                reject(err);
            }
            let url;
            console.log("result from server", body);
            try {
                url = JSON.parse(body).data.url;
                // let url=body.data.url;
                resolve(url);
            } catch (error) {
                reject(error);
            }
        });
    });
}
