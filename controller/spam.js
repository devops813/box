// const commonmodels = require('@leadercodes/modelsnpm');
// const User = commonmodels.models.user


const User = require('../models/User')

module.exports = {

    getAllSpamContactsIDs: async (req, res) => {
        console.log("getAllSpamContactsID");
        const Cuser = await User.findOne({ username: req.params.userName });
        try {
            return res.json({ status: 200, allSpamContactsID: Cuser.spamContactID })
        } catch (error) {
            res.status(500).json({ myMessage: error.message })
        }

    },
    addSpamContactID: async (req, res) => {
        console.log("addSpamContactID");
        try {
            const Cuser = await User.findOne({ username: req.params.userName });
            const contactID = req.body.spamContactID
            await Cuser.spamContactID.push(contactID);
            await Cuser.save();
            return res.json({ status: 200, SpamContactsID: Cuser.spamContactID })
        } catch (error) {
            res.status(500).json({ myMessage: error.message })
        }
    },
    deleteSpamContactID: async (req, res) => {
        console.log("deleteSpamContactID");
        try {
            const Cuser = await User.findOne({ username: req.params.userName });
            const spamContactID1 = req.body.spamContactID;
            await Cuser.spamContactID.pull(spamContactID1)
            await Cuser.save();
            return res.status(200).json({ "massage": "ContactID is deleted" });
        }
        catch (error) {
            res.status(400).json({ "error message": error.massage });
        }
    }
}
//  function checkContactIDInSpam(user, ContactID) {
//     console.log('checkContactIDInSpam');
//     let a = user.spamContactID.find(x => x !== ContactID)
//     return a;
// }
