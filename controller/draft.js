// const commonmodels = require('@leadercodes/modelsnpm');
// const Draft = commonmodels.models.draft
// const User = commonmodels.models.user
// const Conversation = commonmodels.models.conversation




const Draft = require('../models/Draft')
const User = require('../models/User')
const Conversation = require('../models/Conversation')

module.exports = {

    getDrafts: async (req, res) => {
        console.log("inGetDrafts");
        const Cuser = await User.findOne({ username: req.params.username });
        const userID = Cuser._id;
        Draft.find({ user: userID })
            .then((drafts) => {
                res.status(200).json({
                    drafts
                })
            }).catch(error => {
                res.status(500).json({
                    error
                })
            })
    },

    addDraft: async (req, res) => {
        const Cuser = await User.findOne({ username: req.params.username });
        const userID = Cuser._id;
        const { draft } = req.body;
        const newDraft = new Draft(draft);
        newDraft.user = userID;
        newDraft.save().then((result) => {
            console.log("newDraft- result", result)
            Cuser.drafts.push(newDraft._id);
            Cuser.save().then((result2) => {
                res.status(200).json({
                    newDraft,
                    message: 'New draft created'
                })
            }).catch((err) => {
                res.status(500).json({
                    err,
                    message: 'failed to save draft to user'
                })
            })
        }).catch((err) => {
            res.status(500).json({
                err,
                message: 'failed to create the new draft'
            })
        })
    },

    editDraft: (req, res) => {
        const { draft } = req.body;
        console.log("edit draft, draft = ", draft)

        Draft.findById(draft._id).then((result) => {
            result.set(draft);

            result.save().then((result2) => {
                // if (!draft.to || draft.to.length === 0) {
                //     Draft.findByIdAndUpdate(draft._id, { to: [...[]] }).then(() => {
                       
                //     }).catch((err) => { })
                // }
                res.status(200).json({
                    message: 'draft edited',
                    result
                })
            }).catch((err) => {
                res.status(500).json({
                    err,
                    message: 'failed to save edited draft'
                })
            })
        }).catch((err) => {
            res.status(500).json({
                err,
                message: "failed to find draft"
            })
        })
    },

    deleteDraft: (req, res) => {
        const { draftID } = req.body;
        console.log("in deleteDraft, draftID = ", draftID)
        Draft.findByIdAndDelete(draftID).then((result) => {
            console.log("deleteDraft result", result)
            User.updateOne({ _id: result.user }, { $pull: { drafts: draftID } }).then((result2) => {
                console.log("deleteDraft result2", result2);
                res.status(200).json({
                    message: 'draft deleted',
                    draftID
                })
            }).catch((err) => {
                res.status(500).json({
                    err,
                    message: 'failed to delete draft from user'
                })
            })
        }).catch((err) => {
            res.status(500).json({
                err,
                message: 'failed to delete draft'
            })
        })
    }
}