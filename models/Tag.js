
const mongoose = require('mongoose')
const tagSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    index: String,
    title: String,
    description: String,
    color: String,
    source: String,
    contacts: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Contact'
    }]
})

module.exports = mongoose.model('Tag', tagSchema)

