const mongoose = require('mongoose')
const sourceSchema = new mongoose.Schema({
    name: String,
    icon: String,
    lightIcon: String,
    color: String,
    pngIcon: String,
})

module.exports = mongoose.model('Source', sourceSchema)

