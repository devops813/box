// const commonmodels = require('@leadercodes/modelsnpm');
// const User = commonmodels.models.user
// const Notification = commonmodels.models.notification



const Notification = require('../models/Notification');
const User = require('../models/User');
const request = require('request');
const schedule = require('node-schedule');

module.exports = {

    addTokenToNotification: async (req, res) => {
        try {
            const token = req.body.token
            const user = await User.findOne({ username: req.params.userName })
            const flag =await user.notification.find(tok=>tok.token===token)
            if(flag)      
                res.status(200) .json({
                    message: 'token aleady exsist',
                })
            await User.findByIdAndUpdate(user._id, {
                $push: { notification: {
                    token:token,
                    joiningDate:Date.now()
                } }
            }, { new: true }).then((result) => {
                // console.log('result add', result.notification)
                res.status(200).json({
                    message: 'token added',
                    notification: result.notification
                })
            })
        }
        catch (error) {
            res.status(400).send(error.message)
        }
    },
    checkIfTokenIsExsist: async (req, res) => {
        try {
            const token = req.body.token
            const user = await User.findOne({ username: req.params.userName })
            if(user){
                if (user.notification.find(t=>t.token===token)) {
                 res.status(200) .json({
                    message: 'token aleady exsist',
                    statusToken:true
                })
            }   
            else {
                res.status(200) .json({
                    message: 'token is not exsist',
                    statusToken:false
                })
            }
            }     
        }
        catch (error) {
            res.status(400).send(error.message)
        }
    },
    removeTokenFromNotification: async (req, res) => {
        try {
            const token = req.body.token
            // console.log('token', token)
            const user = await User.findOne({ username: req.params.userName })
            if(user){
            await User.findByIdAndUpdate(user._id, {
                $pull: { notification: token }
            }, { new: true }).then((result) => {
                // console.log('result remove', result.notification)
                res.status(200).json({
                    message: 'token remove',
                    notification: result.notification
                })
            })
        }
        }
        catch (error) {
            res.status(400).send('error')
        }
    },
    sendNotification: async (req, res) => {
        const notification=req.body.notification
        const user = await User.findOne({ username: req.params.userName })
        if(user){
            user.notification.forEach(async t => {
                //  console.log('token aaa',t.token)
                body = {
                    "title": notification.title,
                    "body": notification.body,
                    "icon": 'https://box.dev.leader.codes/trytry',
                    "image":notification.image,
                    "fcmToken":  t.token
                }
                const options = {
                    url: 'https://mails.codes/mail/userName/notification',
                    method: 'POST',
                    headers: {
                        "Authorization": 'secretKEY@2021',
                        "Content-Type": "application/json"
                    },
                    json: body,
                    "data": JSON.stringify({ "json": JSON.stringify(body) })
                }
                await request(options, (error, response, body) => {
                    if (error) {
                        console.log(error)
                    }
                    else {
                        res.json({ message: 'success send notification' })
        
                    }
                })
            })  
            

        }
        else {
            // console.log('AAA')
            res.status(404).message('user not found ')
        }
    },
    sendNotaficationToAll: async (req, res) => {
        
    },

    createNotification: async (req, res) => {
        try {
            const newNotification = new Notification(req.body.newNotification)
            const user = await User.findOne({ username: req.params.userName })
            newNotification.manager = user._id
            await newNotification.save()
            await User.findByIdAndUpdate(user._id, {
                $push: { notifications: newNotification._id }
            }, { new: true }).then((result) => {
                res.status(200).json({
                    message: 'notification created',
                    notification: newNotification,
                    // userList: result.notifications
                })
            })
        }
        catch (error) {
            res.status(400).send('error')
        }
    },
    addUserToNotification: async (req, res) => {
        try {
            const notificationId = req.body.notificationId
            // console.log('notificationId', notificationId)
            const user = await User.findOne({ username: req.params.userName })
            // const result = 
            await Notification.findByIdAndUpdate(notificationId, {
                $push: { usersList: user._id }
            }, { new: true }).then((result) => {
                // console.log('result rrr', result)
                res.status(200).json({
                    message: 'user added to notification',
                    notification: result
                })
            })
        }
        catch (error) {
            res.status(400).send('error')
        }
    },
    removeUserFromNotification: async (req, res) => {
        try {
            // 1
            // const user = await User.findOne({ username: req.params.userName })
            // console.log('user  ttt',user._id)
            // const notificationId = req.body.notificationId
            // const notif = await Notification.findOne({ _id: notificationId })
            // console.log('notif lll',notif)
            // if(notif){
            //     await notif.usersList.pull(user._id)
            //     await notif.save()
            //     console.log('notif jjj',notif)

            // }
            // 2
            const notificationId = req.body.notificationId
            // console.log('notification id', notificationId)
            const user = await User.findOne({ username: req.params.userName })
            // console.log('user  ttt', user._id)

            // const result=
            await Notification.findByIdAndUpdate(notificationId, {
                $pull: { usersList: user._id }
            }, { new: true }).then((result) => {
                console.log('result rrr', result)
                res.status(200).json({
                    message: 'user delete from notification',
                    notification: result
                })
            })
            // end
            res.status(200).json({
                message: 'user remove from notification',
                notification: notif
            })
        }
        catch (error) {
            res.status(400).send('error')
        }
    },
    getAllNotaficaionsToManager: async (req, res) => {
        try {
            const user = await User.findOne({ username: req.params.userName });
            // console.log("user notification,", user._id)
            // const notifications = await User.findById(user._id).populate('notifications').notifications
            const notifications = await Notification.find({ manager: user._id })
            // console.log("notifications to user", notifications)
            res.status(200).json({ notifications: notifications[0] })
        }
        catch (error) {
            res.status(400).send('error')
        }
    },
    deleteNotafication: async (req, res) => {
        try {
            // console.log('in delete notification')
            // console.log('req.body.notifId',req.body.notificationId)
            // const notif=Notification.findOne({_id:req.body.notificationId})
            // const user=User.findOne({ _id: notif.manager })
            const user = await User.findOne({ username: req.params.userName })
            // console.log('user',user)
            await user.notifications.pull(req.body.notificationId)
            await user.save();
            // console.log('success delete from user list')

            Notification.deleteOne({ _id: req.body.notificationId }).then((result) => {
                res.status(200).json({
                    message: 'success to delete notafication'
                })
            }).catch(err => {
                res.status(500).json({
                    err,
                    message: 'failed to find notafication'
                })
            })
        }
        catch (error) {
            res.status(400).send('error')
        }

    },
    sendNotafication: (req, res) => {
    }
}





//try sample notification
// app.get("/try", async (req, res, next) => {
//     console.log("arrive to gettttt");
  
  
//     body = {
//       "title": "new conversation",//title of notification
//       "body": "Leader now",//title of notification
//       "icon": 'https://files.codes/uploads/sarac/img/1619336210863__2000_60157eec9e1be.png',
//       "fcmToken": 'ctdnv7fLHOn55Bm_UP3GIe:APA91bHNimf7rGQyom2VZqKTE2ZqVYy8mv4jsc1YEifZbc5_B9upXXs0X56tL4unyp6kFF4WxZN6gWqvKR0eIss7LsrOXKI5qYj4ysMoGtoMSNM2xkofTWabyBlLRqNmPQwjpPAVx73B',
//       "largeIcon": "./Box.png",
//       "largeIconUrl": "./Box.png",
//       "smallIcon": "./Box.png",
//       "content_available": true,
//       "priority": "high",
//     }
  
//     const options = {
//       url: 'https://mails.codes/mail/userName/notification',
//       method: 'POST',
//       headers: {
//         "Authorization": 'secretKEY@2021',//לא לשנות
//         "Content-Type": "application/json"
//       },
//       json: body,
//       "data": JSON.stringify({ "json": JSON.stringify(body) })
//     }
  
//     await request(options, (error, response, body) => {
//       if (error) {
//         console.log(error)
//       }
//       else {
//         console.log('notification sent sccessfuly')
//       }
//     })
  
//   })
  