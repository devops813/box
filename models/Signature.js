const mongoose = require('mongoose')
const signatureSchema = new mongoose.Schema({
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    signatureName: { type: String, unique: true },
    full_name: String,
    role: String,
    email: String,
    phone: String,
    address: String,
    website: { type: String},
    main_color: String,
    text_color: String,
    followTheLeader: {
        display: Boolean
    },
    // text_size: Number,
    align_text: String,
    linkedin_icon: {
        url: String,
        display: Boolean,
        color: { type: String, default: "#3b77b5" }
    },
    twitter_icon: {
        url: String,
        display: Boolean,
        color: { type: String, default: "#55abe1" }
    },
    facebook_icon: {
        url: String,
        display: Boolean,
        color: { type: String, default: "#3b5998" }
    },
    instagram_icon: {
        url: String,
        display: Boolean,
        color: { type: String, default: "#3d719d" }
    },
    youtube_icon: {
        url: String,
        display: Boolean,
        color: { type: String, default: "#ce2827" }
    },
    whatsapp_icon: {
        url: String,
        display: Boolean,
        color: { type: String, default: "#5dc354" }
    },
    upload_photo: {
        url: String,
        display: Boolean
    },
    banner: {
        imgUrl: String,
        CTA: String,
        display: Boolean
    },
    cta_page: {
        type: String
    }
})

module.exports = mongoose.model('Signature', signatureSchema)
