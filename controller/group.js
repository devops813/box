// const commonmodels = require('@leadercodes/modelsnpm');
// const User = commonmodels.models.user

const User = require('../models/User');
const request = require('request');

module.exports = {
    // newGroup: async (req, res) => {
    //     const { group } = req.body;
    //     const newGroup = new Group(group);
    //     const Cuser = await User.findOne({ username: req.params.username });
    //     newGroup.user = Cuser._id;
    //     newGroup.save().then((result) => {
    //         Cuser.groups.push(result._id);
    //         Cuser.save().then((res2) => {
    //             res.json({
    //                 result,
    //                 message: 'success to add new group'
    //             })
    //         }).catch(err => {
    //             res.status(500).json({
    //                 err,
    //                 message: 'failed to add group to user'
    //             })
    //         })
    //     }).catch(err => {
    //         res.status(500).json({
    //             err,
    //             message: 'failed to save new group'
    //         })
    //     })


    // },

    newGroup: (req, res) => {
        const { group } = req.body;
        var gn = false;
        User.findOne({ username: req.params.username }).then((user) => {
            const groups = Array.from([...user.groups])
            groups.forEach(element => {
                if (element.groupName === group.groupName)
                    gn = true;

            })
            if (gn == true)
                return res.json({ groups, message: 'group with the same name exist already' })

            User.findOneAndUpdate({ username: req.params.username }, { $push: { groups: group } }).then((Cuser) => {
                User.findOne({ username: req.params.username }).then((user) => {
                    const groups = Array.from([...user.groups])
                    res.json({
                        groups,
                        message: 'success to add new group'
                    })
                }).catch(err => {
                    res.status(500).json({
                        err,
                        message: 'failed to find user2'
                    })
                })

            }).catch(err => {
                res.status(500).json({
                    err,
                    message: 'failed to add new group'
                })
            })


        }).catch(err => {
            res.status(500).json({
                err,
                message: 'failed to find the user1'
            })
        })

    },

    deleteGroup: (req, res) => {
        const { groupname } = req.body;
        User.findOneAndUpdate({ username: req.params.username },
            {
                $pull:
                    { groups: { groupName: groupname } }
            }).then((Cuser) => {
                res.json({
                    message: 'success to delete the group'
                })
            }).catch(err => {
                res.status(500).json({
                    err,
                    message: 'failed to find user'
                })
            })

    },

    deleteAllGroups: async (req, res) => {
        User.findOne({ username: req.params.username }).then((Cuser) => {
            Cuser.groups = [];
            Cuser.save().then((result) => {
                res.json({
                    message: 'all groups deleted'
                })
            }).catch(err => {
                res.status(500).json({
                    err,
                    message: 'failed to delete the groups'
                })
            })

        }).catch(err => {
            res.status(500).json({
                err,
                message: 'failed to find the user'
            })
        })
    },

    getGroups: async (req, res) => {
        console.log("groupsssssss")
        User.findOne({ username: req.params.username }).then((Cuser) => {
            const groups = Array.from([...Cuser.groups])
            res.json({
                groups
            })
        }).catch(err => {
            res.status(500).json({
                err,
                message: 'failed to get the groups'
            })
        })

    },

    getGroupByName: async (req, res) => {
        const { groupname } = req.body;
        User.findOne({ username: req.params.username }).then((Cuser) => {
            const groups = Array.from([...Cuser.groups])
            var group
            groups.forEach(element => {
                if (element.groupName == groupname) {
                    group = element;
                }
            });
            if (group)
                res.json({
                    group
                })
            else
                res.json({
                    message: 'group is not exist'
                })
        }).catch(err => {
            res.status(500).json({
                err,
                message: 'failed to find user'
            })
        })
    },

    editGroup: async (req, res) => {
        const { groupname } = req.body;
        const { newname } = req.body;
        const { members } = req.body;

        User.findOne({ username: req.params.username }).then((Cuser) => {
            const groups = Array.from([...Cuser.groups])
            var group
            groups.forEach(element => {
                if (element.groupName == groupname) {
                    group = element;
                }
            });
            if (group) {
                if (newname)
                    group.groupName = newname;
                if (members)
                    group.members = members;
                group.save();
                console.log(group);
                Cuser.save();
                res.json({ group });
            }
            else
                res.json({ message: 'group is not exist' })
        }).catch(err => {
            res.status(500).json({
                err,
                message: 'failed to find user'
            })
        })
    }






}
