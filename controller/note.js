// const commonmodels = require('@leadercodes/modelsnpm');
// const User = commonmodels.models.user
// const Note = commonmodels.models.note


const Note = require('../models/Notes')
const User = require('../models/User')

const createNote = async (req, res) => {
    console.log("enterrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr@@@@@@@@@@@@@@@@@@@2");
    try {
        const Cuser = await User.findOne({ username: req.params.username })
        console.log(Cuser);
        const textNote = req.body.textNote
        const indexNote = req.body.indexNote
        const newNote = new Note({
            indexNote: req.body.indexNote,
            userName: Cuser._id,
            createNote: new Date(),
            textNote: req.body.textNote,
            placeX: req.body.placeX,
            placeY: req.body.placeY,
            colors: req.body.colors,
            flagColor: false,
            check: req.body.check
        })
        await newNote.save()
        Cuser.notes.push(newNote);
        console.log(newNote);
        Cuser.save();
        res.json({ "new note": newNote })

    } catch (error) {
        res.status(400).send(error)
    }
}
const getNotesByUserName = async (req, res) => {
    console.log("getFunction@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    try {
        const Cuser = await User.findOne({ username: req.params.username }).
            populate('notes');
        console.log("userCurrent::::::::::::        ", Cuser.notes);
        res.json({ "notes": Cuser.notes })
    } catch (error) {
        res.status(400).send(error)
    }
}
const deleteNotesByUserName = async (req, res) => {
    console.log("@@@@@@@@@@@@@@@@@@@deleteFunction");
    try {
        const Cuser = await User.findOne({ username: req.params.username });
        const u = Cuser._id
        const Cnote = await Note.findOne({ indexNote: req.params.indexNote, userName: u });
        const userId = Cuser._id;
        await User.findOneAndUpdate({ username: req.params.userName },
            { $pull: { notes: Cnote._id } })
        await Cnote.remove()
        res.json({ "note_to_delete": Cnote, "all notes": Cuser.notes })
    } catch (error) {
        res.status(400).send(error)
    }
}
const deleteNoteByMongoId = async (req, res) => {
    console.log("deleteFunction222222222222222");
    const _id = req.params._id
    console.log(_id);
    try {
        const Cnote = await Note.findById(_id);
        await User.findOneAndUpdate({ _id: Cnote.userName }, { $pull: { notes: Cnote._id } })
        await Cnote.remove();
        console.log(Cnote);
        res.json({ "note is deleted": Cnote })
    } catch (error) {
        res.status(400).send(error)
    }
}
const updateNote = async (req, res) => {
    console.log("updateNte@@@@@@@@@@@@@@@@@@@@@@@", req.body);
    const Cuser = await User.findOne({ username: req.params.username });
    // newGoals=JSON.parse(req.body)N
    Note.findOneAndUpdate({ indexNote: req.params.indexNote, userName: Cuser._id },
        { $set: (req.body) }, { new: true })
        .then((user) => {
            // console.log("user.goals", user.goals);
            res.status(200).json({ notes: user.notes, user: user })
        }).catch(error => {
            res.status(500).json({
                error
            })
        })
}

const updateNoteByID = async (req, res) => {
    console.log("hiiiiiiiiiiiiiii", req.body);
    const Cuser = await User.findOne({ username: req.params.username });
    // newGoals=JSON.parse(req.body)N
    Note.findOneAndUpdate({ _id: req.params._id, userName: Cuser._id },
        { $set: (req.body) }, { new: true })
        .then((user) => {
            // console.log("user.goals", user.goals);
            res.status(200).json({ notes: user.notes, user: user })
        }).catch(error => {
            res.status(500).json({
                error
            })
        })
}
module.exports = { createNote, getNotesByUserName, deleteNotesByUserName, deleteNoteByMongoId, updateNote, updateNoteByID }