const commonmodels = require('@leadercodes/modelsnpm');
// const User = commonmodels.models.user
// const Tag = commonmodels.models.tag
// const Conversation = commonmodels.models.conversation
// const Contact = commonmodels.models.contact



const Tag = require('../models/Tag')
const User = require('../models/User')
const Conversation = require('../models/Conversation')
const Contact = require('../models/Contact')

const randomColor = require('randomcolor'); // import the script
const request = require('request')

GmailTag = async (req) => {
    console.log("SDAD")
    const options = {
        method: 'GET',
        url: `https://api.dev.leader.codes/gmailListLabels`,
        headers: { Authorization: req.headers['authorization'] }
    }
    return new Promise(async (resolve, reject) => {
        await request(options, (error, response, body) => {
            console.log("res", response.statusCode, body)
            if (error || response.statusCode != 200)
                reject(error)
            else resolve(body)
        })
    })


}
module.exports = {
    //24 שעות

    newGmailTag: async (req, res) => {
        console.log("#@newGoolgleLable@#")
        let labls = await GmailTag(req)
        let lablsJson = JSON.parse(labls)
        // console.log("labls",labls)
        console.log("lablsJson", lablsJson)

        const Cuser = await User.findOne({ username: req.params.username }).populate('tags');
        const userID = Cuser._id;
        let resulteArr = []
        let ifLablesExist
        await lablsJson.forEach(async label => {
            ifLablesExist = false
            Cuser.tags.forEach(a => {
                console.log(a.title);
                if (a.title === label) ifLablesExist = true;
            })
            if (!ifLablesExist) {
                console.log("notExixst", ifLablesExist)
                let colorlabel = label.color && label.color.backgroundColor ? label.color.backgroundColor : randomColor()
                console.log("color", label)
                let newTag = await new Tag({
                    user: userID,
                    title: label,
                    color: colorlabel,
                    source: 'google'
                })
                newTag.save()
                console.log("newTag", newTag)
                let result2 = await User.updateOne({ _id: userID }, { $push: { tags: newTag } })
                if (!result2)
                    res.status(500).json({ message: 'failed to add tag to user' })

            }
        })


        resulteArr = await Cuser.tags
        res.status(200).json({ message: 'gmail tag created', resulteArr })



    },

    getAllTags: async (req, res) => {
        console.log("inGetAllTags");
        const Cuser = await User.findOne({ username: req.params.username });
        const userID = Cuser._id;

        console.log("userTAGSSSSS,", userID);
        Tag.find({ user: userID })
            .then((tags) => {
                res.status(200).json({
                    tags
                })
            }).catch(error => {
                res.status(500).json({
                    error
                })
            })
    },


    newTag: async (req, res) => {
        const Cuser = await User.findOne({ username: req.params.username });
        const userID = Cuser._id;

        const { tag } = req.body;

        tag.user = userID;
        Tag.insertMany(tag)
            .then((result) => {
                User.updateOne({ _id: result[0].user }, { $push: { tags: result[0]._id } }).then((result2) => {
                    var tag1 = result[0]
                    res.status(200).json({
                        message: 'tag created',
                        tag1
                    })
                }).catch(error => {
                    res.status(500).json({
                        error,
                        message: 'failed to add tag to user'
                    })
                })

            }).catch(error => {
                res.status(500).json({
                    error,
                    message: 'failed to save tag'

                })
            })
    },

    editTag: (req, res) => {
        const { tag } = req.body;
        console.log("tag in edit tag: ", tag)
        Tag.findById(tag._id).then((result) => {
            result.set(tag);
            result.save();
            res.status(200).json({
                message: 'tag updated',
                result
            })
        }).catch(error => {
            res.status(500).json({
                error
            })
        })
    },

    editTags: (req, res) => {
        console.log("in editTags!")
        const { tagsList, color } = req.body;
        console.log(tagsList)
        console.log(color)
        Tag.updateMany({ _id: { $in: tagsList } }, { $set: { color: color } }, { multi: true, new: true }).then((result) => {
            res.status(200).json({
                message: 'tags updated',
                result
            })
        })
            .catch(error => {
                console.log("error", error)
                res.status(500).json({
                    error
                })
            })
    },

    //delete from user and from all its conversation and from its contact with this tag
    //if there is conversation without contact(anonimic)-
    // so how sould I know this conversation connect to user
    //and if I 'll delete all tags with this ID - what if there are static tags?
    deleteTag: (req, res) => {
        console.log('inDeleteTag')
        const { tagID } = req.body;
        Tag.findByIdAndDelete(tagID).then((result) => {
            console.log("result", result);

            User.updateOne({ _id: result.user }, { $pull: { tags: tagID } }).then((result2) => {
                console.log("result2", result2);
                //Contact.find({user:result.user})

                Conversation.updateMany({ tag: tagID }, { tag: null }).then((result3) => {
                    console.log("result3", result3);

                    result.contacts.forEach((contactId) => {
                        Contact.findById(contactId).then((contact) => {
                            console.log("delete tag from contact")
                            console.log(contact)
                            contact.tags = contact.tags.filter(tag => tag != tagID)
                            contact.save()
                            console.log(contact)
                        })
                        .catch(error => {
                            console.log('failed to delete tagId from contact')
                        })
                    })

                    res.status(200).json({
                        message: 'tag deleted',
                        //      result
                    })
                }).catch(error => {
                    res.status(500).json({
                        error,
                        message: 'failed to delete from conversations'
                    })
                })
            }).catch(error => {
                res.status(500).json({
                    error,
                    message: 'failed to delete from user'
                })
            })

        }).catch(error => {
            res.status(500).json({
                error,
                message: 'failed to delete tag'

            })
        })
    },
    deleteTags: async (req, res) => {
        const { tagsID } = req.body;
        let Cuser = {}
        const userID = Cuser._id;
        //works
        Tag.deleteMany({ _id: { $in: tagsID } }).then((result) => {
            console.log(req.params.username);
            User.findOneAndUpdate({ username: req.params.username }, { $pull: { "tags": { $in: tagsID } } }, { multi: true }).then((result2) => {
                console.log("result2", result2.tags);
                console.log("tagsID", tagsID);

                Cuser = result2
                Conversation.updateMany({ user: Cuser._id, tag: tagsID }, { tag: null }).then((result3) => {

                    res.status(200).json({
                        message: 'tag deleted',
                    })
                })
            })
        })


        // await conversationsID.forEach(async function (element, index, conversationsID) {
        //     await Conversation.findOne({ _id: element })
        //         .then(async (result) => {
        //             await result.remove().then((res2) => {
        //                 console.log("arrived to then!!");
        //                 ind = index;

        //             }).catch(error => {
        //                 err = true;
        //                 return res.status(500).json({
        //                     error,
        //                     message: 'failed to delete some or all conversations'
        //                 })
        //             })

        //         }).catch(error => {
        //             err = true;
        //             return res.status(500).json({
        //                 error,
        //                 message: 'failed to find some or all conversations'
        //             })
        //         })
        //     console.log("if!!", err, ind);
        //     if (err == false && ind == length - 1) {
        //         return res.status(200).json({
        //             message: 'conversations deleted'

        //         })
        //     }

        // })
    },

    //not relevant for now
    defaultTags: async (req, res) => {
        const Cuser = await User.findOne({ username: req.params.username });
        const userID = Cuser._id;

        // const userID = req.body;
        const list = [{ title: "New Lead", color: "#A66DFF", user: userID },
        { title: "In Progress", color: "#FEAB06", user: userID },
        { title: "Qualified", color: "#06FE9B", user: userID },
        { title: "Unqualified", color: "#FF6D77", user: userID },
        { title: "Costumer", color: "#06B4FE", user: userID }]
        Tag.insertMany(list).then((result) => {
            res.status(200).json({ result })
        }).catch(error => {
            res.status(500).json({
                error,
                message: 'failed to delete tag'

            })
        })
    }


}