// const commonmodels = require('@leadercodes/modelsnpm');
// const Source = commonmodels.models.source



const Source = require('../models/Source');
const request = require('request');

module.exports = {
    addNewSource: (req, res) => {
        const { source } = req.body;
        const newSource = new Source(source);
        newSource.save().then((result) => {
            res.json({
                message: 'success to add source'
            })
        }).catch((err) => {
            res.status(500).json({
                err,
                message: 'failed to create the source'
            })
        })

    },

    deleteSource: (req, res) => {
        Source.deleteOne({ _id: req.body._id }).then((result) => {
            res.json({
                message: 'success to delete source'

            })
        }).catch(err => {
            res.status(500).json({
                err,
                message: 'failed to find source'
            })
        })

    },


    getSources: async (req, res) => {
        Source.find().then((sources) => {
            res.json({
                sources
            })
        }).catch(err => {
            res.status(500).json({
                err,
                message: 'failed to get the sources'
            })
        })
    },

    editSource: (req, res) => {
        let { source } = req.body
        console.log("sourec to edit:", source)
        Source.findById(source._id).then(async (result) => {
            console.log("result ", result)
            try {
                await result.set(source)
                await result.save()
                res.status(200).json({
                    message: "source updated",
                    source: result
                })
            }
            catch {
                res.status(500).json({
                    message: "failed to set or to save source",
                    sourceToEdit: source
                })
            }
        }).catch((err) => {
            res.status(500).json({
                message: "failed to edit source",
                sourceToEdit: source
            })
        })
    }



}
