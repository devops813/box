const mongoose = require('mongoose')

const userNoteSchema = mongoose.Schema({


    indexNote: {
        type: Number

    },
    userName:
        { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
    ,
    createNote: {
        type: Date,
        default: Date.now

    },
    textNote: {
        type: String

    },
    placeX: {
        type: Number
    },
    placeY: {
        type: Number

    },
    colors: {

        type: String
    },
    flagColor: {
        type: String
    },
    check: {
        type: String
    }


})

module.exports = mongoose.model('Notes', userNoteSchema)